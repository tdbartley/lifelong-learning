from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import os
import numpy as np
import time
from math import sin, cos, radians, pi
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import cv2
from constants import No
from constants import img_folder
from constants import data_folder
from constants import frame_num
from constants import frame_step
from constants import set_num
import cifar10
import cv2 		# used to generate mp4 video 

import PIL 		# for horizontally stacking images

# make_video() combines all frames of a given training/test initial condition into a video 
#	CREDIT: Code fragment taken from Puja Sharma's response in https://stackoverflow.com/questions/33311153/python-extracting-and-saving-video-frames
#	CREDIT: Code for stacking taken from dermen's response in https://stackoverflow.com/questions/30227466/combine-several-images-horizontally-with-python 
def make_video(fig_num, filename, img_folder):
	image_array = []
	fps = 15
	for i in range(fig_num):
		list_im = [(img_folder + "true_" + str(i) + ".png"), (img_folder + "modeling_" + str(i) + ".png")]
		imgs    = [ PIL.Image.open(j) for j in list_im ]
		min_shape = sorted( [(np.sum(j.size), j.size ) for j in imgs])[0][1] 			# resize all images to smallest image dimensions
		imgs_comb = np.hstack( (np.asarray( i.resize(min_shape) ) for i in imgs ) )		# horizontally stack the images
		imgs_comb = PIL.Image.fromarray( imgs_comb)										# save
		imgs_comb.save(img_folder + "combined_" + str(i) + ".png")
	
		img = cv2.imread(img_folder + "combined_" + str(i) + ".png")
		size =  (img.shape[1],img.shape[0])
		img = cv2.resize(img,size)
		image_array.append(img)
	fourcc = cv2.VideoWriter_fourcc(*'mp4v')				# NOTE: *'mp4v' may not work on your OS; look at error message to find the correct string if this fails
	out = cv2.VideoWriter(filename,fourcc, fps, size)
  
	for i in range(len(image_array)):
		out.write(image_array[i])
	out.release()
  
def main():
	make_video(20, "result.mp4", img_folder+"results/")
	
main()