# -*- coding: utf-8 -*-

No = 1 # the number of objects

img_folder = "/homes/ehanson/lifelong/agent/vin/foosball_tracking/track3_img/" # image folder
data_folder = "/homes/ehanson/lifelong/agent/vin/foosball_tracking/track3-targetLocations.txt" # data folder
#img_folder = "/homes/ehanson/lifelong/agent/vin/foosball_tracking/Single_team1_img/" # image folder
#data_folder = "/homes/ehanson/lifelong/agent/vin/foosball_tracking/Single_team1-targetLocations.txt" # data folder
save_folder = "/homes/ehanson/lifelong/agent/vin/saves/"

num_sessions = 1 # number of sessions per maze
frame_num = 943 # The Number of Saved Frame per each simul
frame_step = 1 	# The difference between saved frames
roll_num = 8 	# The Number of Rollout
batch_num = 4
set_num = 1		# number of mazes per set 
set_val = 1#35		# number of sets
fea_num = 2;	# 4 features on the state [x,y,x_vel,y_vel] 
ext_num = 0;	# 4 external inputs (features) [l_input,r_input,u_input,d_input]