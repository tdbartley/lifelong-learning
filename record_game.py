#!/usr/bin/env python

import pygame
import numpy as np
from ple.games.waterworldcircles import WaterWorldCircles
from ple.games.waterworldcircles import process_state
from ple.games.flappybird import FlappyBird
#from ple.games.waterworld import WaterWorld
#from ple.games.waterworld import process_state
from ple import PLE
from os import listdir
from os.path import isfile, join
import os

img_folder = "/homes/ehanson/lifelong/agent/vin/ww_total_img/"
data_folder = "/homes/ehanson/lifelong/agent/vin/ww_total_data/"

if __name__ == "__main__":
	num_sessions = 10
	num_frames = 1000
	mazepath = "/homes/ehanson/lifelong/mazes/9x9_simple_sorted/17/"
	mazes = [f for f in listdir(mazepath) if isfile(join(mazepath,f))]
	
	for maze_idx in range(len(mazes)):
		for session in range(num_sessions):
			game = WaterWorldCircles(maze_file=mazepath+mazes[maze_idx])
			p = PLE(game, fps=30, display_screen=False)
			allowed_actions = p.getActionSet()
			print(allowed_actions)
			p.init()
			for frame in range(num_frames):
				if p.game_over():
					p.reset_game()
					break
				action1 = allowed_actions[len(allowed_actions)-1]								# reset all possible actions to "None"
				action2 = allowed_actions[len(allowed_actions)-1]
				action3 = allowed_actions[len(allowed_actions)-1]
				action4 = allowed_actions[len(allowed_actions)-1]
				toMove = np.random.randint(0,2, size=4)
				toMove[0] = np.random.randint(0,15)
				
				if toMove[0] == 0:													# action 1 is set to l,r,u, or d
					action1 = allowed_actions[np.random.randint(0, len(allowed_actions)-1)]
					if toMove[1] == 0:												# set an action 2, different from action1
						while True:
							action2 = allowed_actions[np.random.randint(0, len(allowed_actions)-1)]
							if action2 != action1:
								break
						if toMove[2] == 0:											# set an action 3
							while True:
								action3 = allowed_actions[np.random.randint(0, len(allowed_actions)-1)]
								if (action3 != action1) and (action3 != action2):
									break
							if toMove[3] == 0:										# set an action 4
								for i in range(0, len(allowed_actions)-1):
									action4 = allowed_actions[i]
									if (action4 != action1) and (action4 != action2) and (action4 != action3):
										break
				#print(action1,action2,action3,action4)
				p.act(action1,action2,action3,action4)
				#print(process_state(p.game.getGameState()))
				#observation = p.getScreenRGB()											# get image
				#print(np.shape(observation))
				
				if not os.path.exists(img_folder+"maze_idx_"+str(maze_idx)+"/"+"session_"+str(session)+"/"): 				# make directory to store images
					os.makedirs(img_folder+"maze_idx_"+str(maze_idx)+"/"+"session_"+str(session)+"/")
				if not os.path.exists(data_folder+"maze_idx_"+str(maze_idx)+"/"+"session_"+str(session)+"/"): 				# make directory to store images
					os.makedirs(data_folder+"maze_idx_"+str(maze_idx)+"/"+"session_"+str(session)+"/")
					
				p.saveScreen(img_folder+"maze_idx_"+str(maze_idx)+"/"+"session_"+str(session)+"/"+str(frame)+".png")
				np.save(data_folder+"maze_idx_"+str(maze_idx)+"/"+"session_"+str(session)+"/"+str(frame), process_state(p.game.getGameState()))
		# DECONSTRUCT P HERE
	
	"""game = WaterWorldCircles()
    #game = FlappyBird()
    p = PLE(game, fps=30, display_screen=True)

    allowed_actions = p.getActionSet()
    print(allowed_actions)
    p.init() 

    # for maze in mazes:
    #     for session in sessions:
    for i in range(1000):
        if p.game_over():
            p.reset_game()

        observation = p.getScreenRGB()
        action1 = allowed_actions[np.random.randint(0, len(allowed_actions))]
        action2 = allowed_actions[np.random.randint(0, len(allowed_actions))]
        p.act(action1)
        print(process_state(p.game.getGameState())[:4])"""
