# -*- coding: utf-8 -*-

#img_folder = "/homes/ehanson/lifelong/agent/vin/ww_img/" 			# image folder
#data_folder = "/homes/ehanson/lifelong/agent/vin/ww_data/" 		# data folder
img_folder = "/homes/ehanson/lifelong/agent/vin/ww_total_img/" 		# image folder
data_folder = "/homes/ehanson/lifelong/agent/vin/ww_total_data/"	# data folder
save_folder = "/homes/ehanson/lifelong/agent/vin/saves/"
load_sess = "saved_model16.ckpt"
save_sess = "saved_model_VIN1.ckpt"#"saved_model18.ckpt" #"temp.ckpt" 						# save file; must end in ".ckpt"; CARE to not overwrite a previously trained model
test_maze = 325														# maze_number of the maze used for testing; 325 is good 

No = 18 					# the number of objects
num_sessions = 2 			# number of sessions per maze
frame_num = 1000 			# The Number of Saved Frame per each simul
frame_step = 1 				# The difference between saved frames
roll_num = 8 				# The number of rollout frames (past the 6 input frames) 
batch_num = 4				# number of batches per tensor feed 
set_num = 30				# number of mazes per set 
set_val = 10				# number of sets
fea_num = 4;				# 4 features on the state [x,y,x_vel,y_vel] 
ext_num = 4;				# 4 external inputs (features) [l_input,r_input,u_input,d_input]
height = 144				# height of images
weight = 144				# width of images 
col_dim = 3					# size of pixel color dimension (ie 3 for rgb)

velo_gain = 0.05			# constant to calculated displacement from velocity; used to generate resulting test video
pos_norm_const = 144.0		# constant to divide all position/velocity values for normalization; this can be improved to provide different values for pos and vel
num_test_inputs = 60		# number of inputs to the tensor for testing; NOTE: make sure that [roll_num * num_test_inputs <= frame_num + last_frame_offset]
ext_input_val = 100			# value of external inputs (ie up,down,left,right)