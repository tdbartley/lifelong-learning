from ple.games.waterworld import WaterWorld
from ple import PLE

game = WaterWorld(width=256, height=256, num_creeps=4, mode="standard")

p = PLE(game, fps=30, display_screen=True, force_fps=False)
p.init()

#print(p.getActionSet())

nb_frames = 1024
reward = 0.0

for f in range(nb_frames):
    if p.game_over(): #check if the game is over
        p.reset_game()

    obs = p.getScreenRGB()
    action = None
    reward = p.act(action)
