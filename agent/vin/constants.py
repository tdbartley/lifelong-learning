# -*- coding: utf-8 -*-

No = 3 # the number of object
img_folder = "img/" # image folder
data_folder = "data/" # data folder
frame_num = 50 # The Number of Saved Frame per each simul
frame_step = 1 # The difference between saved frames
roll_num = 20 # The Number of Rollout
set_num = 10 # The Number of set; 10 different sets in CIFAR dataset
fea_num = 5;	# 5 features on the state [mass,x,y,x_vel,y_vel] 

