This file defines what each "saver" and "training_metadata" file corresponds to.
NOTE: All saved models below saver11 was lost due to them being stored in Bahamut's TEMP directory.
They now exist in a designated folder. Also, these descriptions are progressive; so any improvements
made by a previous saved model are carried onto future saved models (unless otherwise stated)

Saved Model Name	|	.mp4 and .csv suffix	|	Description
---------------------------------------------------------------
saver1				|	long_orig				| 	non-standardized relational NN. All walls have
					|							|	unique IN vectors to the agent. 8 rollout
saver2				|	long 					|	standardized relational NN. 8 rollout
saver3				|	long_orig_20			|	non-standardized relational NN. 20 rollout
saver4				|	long_20					|	standardized relational NN. 20 rollout
saver5				|	5						|	same as saver4, but with ex_input = 20
saver6				|	long_fix				|	PROPERLY IMPLEMENTED STANDARDIZED NN (used 
					|							|	local_scope), ex_input=20
saver7				|	7						|	normalized position and velocity state inputs
saver8				|	8						|	normalized, ext_input = 100
saver9				|	9						|	VE + SD added
saver10				|	10						|	IN with applied State Encoder (SE), ext_input = 100
saverVE				|	VE						|	just the VE 
saver11 			|	11						|	IN with new large data set
saver11_2			|	11_2					|	SE + IN, ext_input = 5
VE3					|	VE3						|	base VE trained with new data
VE4					|	VE4 					|	VIN paper version VE with light data set
VE5 				|	VE5 					|	VIN paper VE with large data set
FB1					|	FB1 					|	track3 DVS VE 
FB2 				|	FB2 					|	track3 orig VE 
saver12				|	12						|	SE with large data set; UNMATCHED ROLLOUT VAL [BAD]
saver13				|	13						|	BAD LIKE 12
VE8					|	VE8 					|	new error calculation technique (smallest err calc.)
					|							|	[BAD]
saver14				|	14						|	rollout=20 ; gradient descent explosion; [BAD]
saver15				|	15						|	rollout=8; NO relational dynamics; light data set
saver16				|	16						|	saver15, but with large data set
saver17				|	17						|	saver15, but WITH relational dynamics