from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import copy
import tensorflow as tf

import math
import matplotlib.image as mpimg
import numpy as np
import time
from vin import VE, DP, SD
from physics_engine import make_video, make_image2
from constants import No,img_folder,data_folder,frame_num, set_num, roll_num, fea_num
import PIL 		# for horizontally stacking images
import cv2 		# used to generate mp4 video 

FLAGS = None
frames_generated = 20				# frames generated for both true and modeling images (must be smaller value of the two)

def variable_summaries(var,idx):
  """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
  with tf.name_scope('summaries_'+str(idx)):
    mean = tf.reduce_mean(var)
    tf.summary.scalar('mean', mean)
    with tf.name_scope('stddev'):
      stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
    tf.summary.scalar('stddev', stddev)
    tf.summary.scalar('max', tf.reduce_max(var))
    tf.summary.scalar('min', tf.reduce_min(var))
    tf.summary.histogram('histogram', var)
	
# make_result_video() combines all frames of a given training/test initial condition into a video 
#	CREDIT: Code fragment taken from Puja Sharma's response in https://stackoverflow.com/questions/33311153/python-extracting-and-saving-video-frames
#	CREDIT: Code for stacking taken from dermen's response in https://stackoverflow.com/questions/30227466/combine-several-images-horizontally-with-python 
def make_result_video(fig_num, filename, img_folder):
	image_array = []
	fps = 15
	for i in range(fig_num):
		list_im = [(img_folder + "true_" + str(i) + ".png"), (img_folder + "modeling_" + str(i) + ".png")]
		imgs    = [ PIL.Image.open(j) for j in list_im ]
		min_shape = sorted( [(np.sum(j.size), j.size ) for j in imgs])[0][1] 			# resize all images to smallest image dimensions
		imgs_comb = np.hstack( (np.asarray( i.resize(min_shape) ) for i in imgs ) )		# horizontally stack the images
		imgs_comb = PIL.Image.fromarray( imgs_comb)										# save
		imgs_comb.save(img_folder + "combined_" + str(i) + ".png")
	
		img = cv2.imread(img_folder + "combined_" + str(i) + ".png")
		size =  (img.shape[1],img.shape[0])
		img = cv2.resize(img,size)
		image_array.append(img)
	fourcc = cv2.VideoWriter_fourcc(*'mp4v')				# NOTE: *'mp4v' may not work on your OS; look at error message to find the correct string if this fails
	out = cv2.VideoWriter(filename,fourcc, fps, size)
  
	for i in range(len(image_array)):
		out.write(image_array[i])
	out.release()

def rollout_DP(prev_out,cur_in):
  # rolling DP to roll_num
  S1,S2,S3,S4=tf.unstack(prev_out,4,0);
  S_pred=DP(S1,S2,S3,S4,FLAGS);								# [4,3,64]; apply DP function **in vin.py
  res=tf.stack([S2,S3,S4,S_pred],0);						# [4,4,3,64]
  return res;

def train():
  # Architecture Definition
  # Frame
  F = tf.placeholder(tf.float32, [None,6,FLAGS.height,FLAGS.weight,FLAGS.col_dim], name="F");	# Inserts a placeholder for a tensor (F) that will be always fed; 
																								# "None" indicates that the first dimension, corresponding to the batch size, can be of any size
  F1,F2,F3,F4,F5,F6 = tf.unstack(F,6,1);														# Unpacks the given dimension of a rank-R tensor (F) into 6 rank-(R-1) tensors (F1,F2,F3,F4,F5,F6) across axis 1
																								# Now, F1 = [F_0,F_1,F_2,F_3], F2 = [F_1,F_2,F_3,F_4]
  # Future Data
  label = tf.placeholder(tf.float32, [None,8,FLAGS.No,4], name="label");				# Inserts a placeholder for a tensor (label) that will be always fed
  label_part = tf.unstack(label,8,1);													# [8,4,3,4]; Unpacks the given dimension of a rank-R tensor (label) into 8 rank-(R-1) tensors (label_part) across dimension 1
																						# Now, label_part_1 = [F_0,...F_4], label_part_2 = [F_1,...,F5] and so on (up to 8 batches)
  # State Code
  S_label = tf.placeholder(tf.float32, [None,4,FLAGS.No,4], name="S_label");
  S_label_part = tf.unstack(S_label,4,1);												# [Batch[4],Frame[4],3,4]
  # discount factor
  df = tf.placeholder(tf.float32,[],name="DiscountFactor");								# just a number
  # x and y coordinate channels
  x_cor = tf.placeholder(tf.float32, [None,FLAGS.height,FLAGS.weight,1], name="x_cor");
  y_cor = tf.placeholder(tf.float32, [None,FLAGS.height,FLAGS.weight,1], name="y_cor");

  # Visual Encoder
  S1,S2,S3,S4 = VE(F1,F2,F3,F4,F5,F6,x_cor,y_cor,FLAGS); 											# [4,3,64]; Visual Encoder defined in vin.py  

  # Rolling Dynamic Predictor
  roll_in = tf.identity(tf.Variable(tf.zeros([roll_num]),dtype=tf.float32));						# create variable 0 tensor of dimension number_of_rollout (20); return it with name "roll_in"
  roll_out = tf.scan(rollout_DP,roll_in,initializer=tf.stack([S1,S2,S3,S4],0));						# [20,4,4,3,64]; repeatedly call rollout_DP on values of roll_in; *values* is unpacked roll_in along axis 0
																									# initializer is the initial value for prev_out (return value) specifies output format of rollout_DP
  S_pred = tf.unstack(roll_out,4,1)[-1]; 															# [20,4,3,64]; grab S_pred from the stack generated by rollout_DP
  S_pred = tf.reshape(tf.stack(tf.unstack(S_pred,FLAGS.batch_num,1),0),[-1,FLAGS.No,FLAGS.Ds]);		# [80,3,64]
  
  # State Decoder
  S = tf.concat([S1,S2,S3,S4,S_pred],0);															# [96,3,64]
  out_sd = SD(S,FLAGS);                     														# [96,3,4]; State Decoder defined in vin.py  
  S_est = np.zeros(4,dtype=object);																	# [4,4,3,4]; State_estimate; 4 mini-batches of 4 (ie 0-3, 4-7, 8-11, 12-15)
  for i in range(4):
    S_est[i] = tf.slice(out_sd,[FLAGS.batch_num*i,0,0],[FLAGS.batch_num,-1,-1]);					
  label_pred = tf.reshape(tf.slice(out_sd,[FLAGS.batch_num*4,0,0],[-1,-1,-1]),[FLAGS.batch_num,roll_num,FLAGS.No,4]);	# [4,20,3,4]
  label_pred8 = tf.unstack(label_pred,roll_num,1)[:8];												# [8,4,3,4]; select only 8 of the 20 label_pred
  
  # loss and optimizer (mse = Mean Squared Error - average squared difference)
  mse = df*tf.reduce_mean(tf.reduce_mean(tf.square(label_pred8[0]-label_part[0]),[1,2])); 			# find mse across axes 1 & 2 (ie across objects and features) #******* print("mse shape:",mse.shape.as_list())
  print("SHAPPPEEE:",tf.square(label_pred8[0]-label_part[0]))
  for i in range(1,8):																				
    mse += (df**(i+1))*tf.reduce_mean(tf.reduce_mean(tf.square(label_pred8[i]-label_part[i]),[1,2]));	# discount factor compounds (lessens) with each batch compared
  mse = mse/8;																						# total mean
  ve_loss = tf.reduce_mean(tf.reduce_mean(tf.square(S_est[0]-S_label_part[0]),[1,2]));				# Visual encoder loss; find mse across objects and features
  for i in range(1,4):
    ve_loss += tf.reduce_mean(tf.reduce_mean(tf.square(S_est[i]-S_label_part[i]),[1,2]));
  ve_loss = ve_loss/4;																				# total VE loss
  total_loss = mse + ve_loss;																		# total loss
  optimizer = tf.train.AdamOptimizer(0.0005); 														# Adam algorithm optimizer; learning rate of 0.0005
  trainer = optimizer.minimize(total_loss);															# set to minimize total_loss; affects all variables defined with tf.get_variable
  
  # tensorboard; enables visualization of the data (must have access to a local web browser)
  # configure Xming (ssh x11 forwarding) to use the web-browser if running this code on a remote computer
  """
  params_list=tf.global_variables();
  for i in range(len(params_list)):
    variable_summaries(params_list[i],i);
  """
  tf.summary.scalar('tr_loss',total_loss);						# Outputs a Summary protocol buffer containing a single scalar value
  merged = tf.summary.merge_all();								# single op that generates all the summary data
  writer = tf.summary.FileWriter(FLAGS.log_dir);				# Writes Summary protocol buffers to event files

  sess = tf.InteractiveSession();								# TensorFlow Session for use in interactive contexts, such as a shell
  tf.global_variables_initializer().run();						# Returns an Op that initializes global variables

  # Get Training Image and Data 
  total_img = np.zeros((FLAGS.set_num,int(frame_num),FLAGS.height,FLAGS.weight,FLAGS.col_dim),dtype=float);		# total_img[num_of_CIFAR_sets[10],num_frames[50],height[32],weight[32],color-channel[4]]
  for i in range(FLAGS.set_num):
    for j in range(int(frame_num)):
      total_img[i,j] = mpimg.imread(img_folder+"train/"+str(i)+'_'+str(j)+'.png')[:,:,:FLAGS.col_dim];			# load all training images; only color-channel[0,2] are used; color-channel[3] is still 0
  total_data = np.zeros((FLAGS.set_num,int(frame_num),FLAGS.No*fea_num),dtype=float);							# total_data[num_of_CIFAR_sets[10],num_frames[50],object_features[15]]
																												# object_features[15] is organized as [m1, x1, y1, x_vel1, y_vel1, m2, x2,..., y_vel3] for objects 1-3
  for i in range(FLAGS.set_num):
    f = open(data_folder+"train/"+str(i)+".csv","r");
    total_data[i] = [line[:-1].split(",") for line in f.readlines()];											# load all training data frame-by-frame for each set
  total_data = np.reshape(total_data,[FLAGS.set_num,int(frame_num),FLAGS.No,fea_num]); 							# RESHAPE total_data[num_of_CIFAR_sets[10],num_frames[50],num_objects[3],num_features[5]]

  # reshape img and data
  input_img = np.zeros((FLAGS.set_num*(int(frame_num)-14+1),6,FLAGS.height,FLAGS.weight,FLAGS.col_dim),dtype=float);	# [set_num[10]*Frames[37],F1...F6,height[32],weight[32],color_dim[4]]
  output_label = np.zeros((FLAGS.set_num*(int(frame_num)-14+1),8,FLAGS.No,4),dtype=float);							
  output_S_label = np.zeros((FLAGS.set_num*(int(frame_num)-14+1),4,FLAGS.No,4),dtype=float);
  for i in range(FLAGS.set_num):
    for j in range(int(frame_num)-14+1):
      input_img[i*(int(frame_num)-14+1)+j] = total_img[i,j:j+6];														# [10*37,6,32,32,4]; gather img data from frames (0-36) in batches of 6 and place into input_img[j]
      output_label[i*(int(frame_num)-14+1)+j] = np.reshape(total_data[i,j+6:j+14],[8,FLAGS.No,fea_num])[:,:,1:5];		# [37,8,3,4]; collect frames (6-50) in batches of 8 and keep x,y,x_vel,y_vel; [Batches[37],Frames[8],num_object[3],features[4]]
      output_S_label[i*(int(frame_num)-14+1)+j] = np.reshape(total_data[i,j+2:j+6],[4,FLAGS.No,fea_num])[:,:,1:5];		# [37,4,3,4]; collect frames (2-42) in batches of 4 and keep x,y,x_vel,y_vel

  # shuffle ***************************----------- Start here and advance to *beta* section to go over how loss is calculated****
  tr_data_num = int(len(input_img)*1);	# 10*37
  val_data_num = int(len(input_img)*0);
  total_idx = list(range(len(input_img)));np.random.shuffle(total_idx);													# shuffle indices for all images of all sets
  mixed_img = input_img[total_idx];mixed_label=output_label[total_idx];mixed_S_label=output_S_label[total_idx];
  tr_data = mixed_img[:tr_data_num];tr_label=mixed_label[:tr_data_num];tr_S_label=mixed_S_label[:tr_data_num];
  val_data = mixed_img[tr_data_num:(tr_data_num+val_data_num)];val_label=mixed_label[tr_data_num:(tr_data_num+val_data_num)];val_S_label=mixed_S_label[tr_data_num:(tr_data_num+val_data_num)];

  # x-cor and y-cor setting
  nx, ny = (FLAGS.weight, FLAGS.height);
  x = np.linspace(0, 1, nx);							# distribute coordinates on [0,1) with 32 increments
  y = np.linspace(0, 1, ny);
  xv, yv = np.meshgrid(x, y);							# [32,32]; xv and yv have matching indices for each unique coordinate point
  xv = np.reshape(xv,[FLAGS.height,FLAGS.weight,1]);	# [32,32,1]
  yv = np.reshape(yv,[FLAGS.height,FLAGS.weight,1]);
  xcor = np.zeros((FLAGS.batch_num*5,FLAGS.height,FLAGS.weight,1),dtype=float);		# [20,32,32,1]
  ycor = np.zeros((FLAGS.batch_num*5,FLAGS.height,FLAGS.weight,1),dtype=float);
  for i in range(FLAGS.batch_num*5):												# for i in range(20); each is a mini-batch
    xcor[i] = xv; ycor[i] = yv;

  # beta
  beta = int(FLAGS.max_epoches*0.05);
  # training
  for i in range(FLAGS.max_epoches):
    #df_value=1-math.exp(-1*i/beta);
    df_value = 1;
    tr_loss = 0;
    tr_loss2 = 0;
    for j in range(int(len(tr_data)/FLAGS.batch_num)):								# 370 / 4
      batch_data = tr_data[j*FLAGS.batch_num:(j+1)*FLAGS.batch_num];
      batch_label = tr_label[j*FLAGS.batch_num:(j+1)*FLAGS.batch_num];
      batch_S_label = tr_S_label[j*FLAGS.batch_num:(j+1)*FLAGS.batch_num];
      if(j == 0):
        estimated,summary,tr_loss_part,tr_loss_part2,_=sess.run([label,merged,mse,ve_loss,trainer],feed_dict={F:batch_data,
																												label:batch_label,
																												S_label:batch_S_label,
																												x_cor:xcor,
																												y_cor:ycor,
																												df:df_value});
        writer.add_summary(summary,i);
      else:
        tr_loss_part,tr_loss_part2,_=sess.run([mse,ve_loss,trainer],feed_dict={F:batch_data,
																				label:batch_label,
																				S_label:batch_S_label,
																				x_cor:xcor,
																				y_cor:ycor,
																				df:df_value});
      tr_loss += tr_loss_part;
      tr_loss2 += tr_loss_part2;
    tr_idx = list(range(len(tr_data)));np.random.shuffle(tr_idx);
    tr_data = tr_data[tr_idx];
    tr_label = tr_label[tr_idx];
    tr_S_label = tr_S_label[tr_idx];
    print("Epoch "+str(i+1)+" Training mse: "+str(tr_loss/(int(len(tr_data)/FLAGS.batch_num)))+" Training ve loss: "+str(tr_loss2/int(len(tr_data)/FLAGS.batch_num)));
  
  # Get Test Image and Data 
  ts_img = np.zeros((1,int(frame_num),FLAGS.height,FLAGS.weight,FLAGS.col_dim),dtype=float);
  for i in range(1):
    for j in range(int(frame_num)):
      ts_img[i,j] = mpimg.imread(img_folder+"test/"+str(i)+"_"+str(j)+'.png')[:,:,:FLAGS.col_dim];
  ts_data = np.zeros((1,int(frame_num),FLAGS.No*fea_num),dtype=float);										# [1,Frames[50],15]
  for i in range(1):																						# currently only test0 is being generated
    f = open(data_folder+"test/"+str(i)+".csv","r");
    ts_data[i] = [line[:-1].split(",") for line in f.readlines()];											# read in test data raw by row and column
  
  # reshape img and data
  input_img = np.zeros((1*(int(frame_num)-14+1),6,FLAGS.height,FLAGS.weight,FLAGS.col_dim),dtype=float);	# input_img[Frames[37],F1...F6,height[32],weight[32],color-channel[4]]
  output_label = np.zeros((1*(int(frame_num)-14+1),8,FLAGS.No,4),dtype=float);	 							# output_label[Frames[37],8,num_objects[3],4]
  output_S_label = np.zeros((1*(int(frame_num)-14+1),4,FLAGS.No,4),dtype=float);							# output_S_label[Frames[37],4,num_objects[3],4]
  for i in range(1):																						# i corresponds to CIFAR set number
    for j in range(int(frame_num)-14+1):
      input_img[i*(int(frame_num)-14+1)+j] = total_img[i,j:j+6]; 											# [37,6,32,32,4]; gather img data from frames (0-36) in batches of 6 and place into input_img[j]
      output_label[i*(int(frame_num)-14+1)+j] = np.reshape(ts_data[i,j+6:j+14],[8,FLAGS.No,fea_num])[:,:,1:5]; 	# [37,8,3,4]; collect frames (6-50) in batches of 8 and keep x,y,x_vel,y_vel; [Batches[37],Frames[8],num_object[3],features[4]]
      output_S_label[i*(int(frame_num)-14+1)+j] = np.reshape(ts_data[i,j+2:j+6],[4,FLAGS.No,fea_num])[:,:,1:5];	# [37,4,3,4]; collect frames (2-42) in batches of 4 and keep x,y,x_vel,y_vel

  xy_origin = output_label[:(int(frame_num)-14+1-4+1),0,:,0:2];
  xy_estimated = np.zeros((roll_num,No,2),dtype=float);
 
  # Rollout; begin feeding values into 
  posi = sess.run(label_pred,feed_dict={F:input_img[0:4],													# [4,6,32,32,4]; Axis 0 is batch number; ie, batch 0 is {F1,...,F6 = Frames[0-5]}, batch 1 is {F1,...,F6 = Frames[1-6], ...}
										label:output_label[0:4],     										# [4,8,3,4]
										S_label:output_S_label[0:4],										# [4,4,3,4]
										x_cor:xcor,															# [20,32,32,1]
										y_cor:ycor,															# [20,32,32,1]
										df:1.0})[0];  			#*************
  #xy_estimated=posi[:,:,:2];		#*******NOTE: figure out why this is not equivalent to below
  velo = posi[:,:,2:4];
  xy_estimated[0] = output_S_label[3][3][:,:2]+velo[0]*0.01;
  for i in range(1,len(posi)):
    xy_estimated[i] = xy_estimated[i-1]+velo[i]*0.01;
  
  # Saving images and video data
  print("Image Making");
  make_image2(xy_origin,img_folder+"results/","true");
  make_image2(xy_estimated,img_folder+"results/","modeling");
  make_result_video(frames_generated, "result.mp4", img_folder+"results/")
  print("Done");

def main(_):
  FLAGS.log_dir += str(int(time.time()));
  if tf.gfile.Exists(FLAGS.log_dir):				# make sure gfile doesnt already exist
    tf.gfile.DeleteRecursively(FLAGS.log_dir)
  tf.gfile.MakeDirs(FLAGS.log_dir)
  FLAGS.No = No;
  FLAGS.set_num = set_num;
  FLAGS.height = 32;
  FLAGS.weight = 32;
  FLAGS.col_dim = 4;
  train()			#************

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--log_dir', type=str, default='/tmp/vin/logs/',
                      help='Summaries log directry')
  parser.add_argument('--batch_num', type=int, default=4,
                      help='The number of data on each mini batch')
  parser.add_argument('--max_epoches', type=int, default=1,			# default=80,000; ~30 min runtime per 200 epoch
                      help='Maximum limitation of epoches')
  parser.add_argument('--Ds', type=int, default=64,					# default = 64
                      help='The State Code Dimension')
  parser.add_argument('--fil_num', type=int, default=128,
                      help='The Number of filters')

  FLAGS, unparsed = parser.parse_known_args()
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
