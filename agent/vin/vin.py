from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import copy
import tensorflow as tf

import numpy as np
import time

# conv_variable() returns randomly generated weight and bias tensors of a given weight_shape[4]
# weight_shape[4] is of form [weight, height, number_input_channels, number_output_channels]
def conv_variable(weight_shape):
  w = weight_shape[0]
  h = weight_shape[1]
  input_channels  = weight_shape[2]				
  output_channels = weight_shape[3]				# number of filters
  d = 1.0 / np.sqrt(input_channels * w * h)		############################# What is this equation? What is d?
  bias_shape = [output_channels]
  weight = tf.Variable(tf.random_uniform(weight_shape, minval=-d, maxval=d))	# generates a tensor with the shape of weight_shape and uniformly distributed random values [-d,d)
																				# tf.Variable contains a persistent tensor; values can be altered using special operations; exists outside the context of a single session.run
  bias   = tf.Variable(tf.random_uniform(bias_shape,   minval=-d, maxval=d))	# randomly generated values for bias
  return weight, bias

# conv2d() computes a 2D convolution given a 4D input and filter tensors
def conv2d(x, W, stride=1):
  return tf.nn.conv2d(x, W, strides = [1, stride, stride, 1], padding = "SAME")

# maxpool2d() performs the k x k max pooling of input tensor x 
def maxpool2d(x, k=2):
  return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME')

def IPE_r1(F1F2,F2F3,F3F4,F4F5,F5F6,x_cor,y_cor,FLAGS):
  img_pair=tf.concat([F1F2,F2F3,F3F4,F4F5,F5F6],0);					# [Batch*5,32,32,8]; concatenate tensor pairs along axis 0 (batch number);###### What does Index 0 look like now?
  # First 2 layer conv (kernel size 10 and 4 channels)
  w_1_1,b_1_1=conv_variable([10,10,FLAGS.col_dim*2,4]);
  h_1_1=tf.nn.relu(conv2d(img_pair,w_1_1,1)+b_1_1);
  w_1_2,b_1_2=conv_variable([10,10,4,4]);
  h_1_2=tf.nn.relu(conv2d(h_1_1,w_1_2,1)+b_1_2);					# [Batch*5,32,32,4]
  # Second 2 layer conv (kernel size 3 and 16 channels)
  w_2_1,b_2_1=conv_variable([3,3,FLAGS.col_dim*2,16]);
  h_2_1=tf.nn.relu(conv2d(img_pair,w_2_1,1)+b_2_1);
  w_2_2,b_2_2=conv_variable([3,3,16,16]);
  h_2_2=tf.nn.relu(conv2d(h_2_1,w_2_2,1)+b_2_2);					# [Batch*5,32,32,16]
  en_pair=tf.concat([h_1_2,h_2_2],3);								# [Batch*5,32,32,4+16]
  # Third 2 layer conv (kernel size 3 and 16 channels)
  w_3_1,b_3_1=conv_variable([3,3,4+16,16]);
  h_3_1=tf.nn.relu(conv2d(en_pair,w_3_1,1)+b_3_1);
  w_3_2,b_3_2=conv_variable([3,3,16,16]);
  h_3_2=tf.nn.relu(conv2d(h_3_1,w_3_2,1)+b_3_2);					# [Batch*5,32,32,16]
  # Inject x and y coordinate channels
  h_3_2_x_y=tf.concat([h_3_2,x_cor,y_cor],3);						# [Batch*5,32,32,18]; ww = [Batch*5,144,144,16]
  # Fourth conv and max-pooling layers to unit height and width
  fil_num=16;
  w_4_1,b_4_1=conv_variable([3,3,16+2,fil_num]);
  h_4_1=tf.nn.relu(conv2d(h_3_2_x_y,w_4_1,1)+b_4_1);				# [Batch*5,32,32,16]
  h_4_1=maxpool2d(h_4_1);											# [Batch*5,16,16,16]; ww = [Batch*5,72,72,16]
  w_4_2,b_4_2=conv_variable([3,3,fil_num,fil_num]);
  h_4_2=tf.nn.relu(conv2d(h_4_1,w_4_2,1)+b_4_2);					# [Batch*5,16,16,16]
  h_4_2=maxpool2d(h_4_2);											# [Batch*5,8,8,16]; ww = [Batch*5,36,36,16]
  w_4_3,b_4_3=conv_variable([3,3,fil_num,fil_num]);
  h_4_3=tf.nn.relu(conv2d(h_4_2,w_4_3,1)+b_4_3);					# [Batch*5,8,8,16]
  h_4_3=maxpool2d(h_4_3);											# [Batch*5,4,4,16]; ww = [Batch*5,18,18,16]
  w_4_4,b_4_4=conv_variable([3,3,fil_num,fil_num]);
  h_4_4=tf.nn.relu(conv2d(h_4_3,w_4_4,1)+b_4_4);
  h_4_4=maxpool2d(h_4_4);											# ww = [Batch*5,9,9,16]
  
  w_4_5,b_4_5=conv_variable([3,3,fil_num,fil_num*2]);
  h_4_5=tf.nn.relu(conv2d(h_4_4,w_4_5,1)+b_4_5);
  h_4_5=maxpool2d(h_4_5,k=3)										# ww = [Batch*5,3,3,32]
  fil_num=32
  w_4_6,b_4_6=conv_variable([3,3,fil_num,fil_num]);
  h_4_6=tf.nn.relu(conv2d(h_4_5,w_4_6,1)+b_4_6);
  h_4_6=maxpool2d(h_4_6,k=3)										# ww = [Batch*5,1,1,32]
  
  """w_4_4,b_4_4=conv_variable([3,3,fil_num,fil_num*2]);
  h_4_4=tf.nn.relu(conv2d(h_4_3,w_4_4,1)+b_4_4);
  fil_num=32;
  h_4_4=maxpool2d(h_4_4);
  w_4_5,b_4_5=conv_variable([3,3,fil_num,fil_num]);
  h_4_5=tf.nn.relu(conv2d(h_4_4,w_4_5,1)+b_4_5);
  h_4_5=maxpool2d(h_4_5)"""
  res_pair=tf.reshape(h_4_6,[-1,fil_num]);							# [Batch*5,32]
  pair1=tf.slice(res_pair,[0,0],[FLAGS.batch_num,-1]);				# [Batch,32]
  pair2=tf.slice(res_pair,[FLAGS.batch_num,0],[FLAGS.batch_num,-1]);
  pair3=tf.slice(res_pair,[FLAGS.batch_num*2,0],[FLAGS.batch_num,-1]);
  pair4=tf.slice(res_pair,[FLAGS.batch_num*3,0],[FLAGS.batch_num,-1]);
  pair5=tf.slice(res_pair,[FLAGS.batch_num*4,0],[FLAGS.batch_num,-1]);
  return pair1,pair2,pair3,pair4,pair5;

# IPE_r2 is an Image Pair Encoder, modified to take 5 pairs of images and output 5 corresponding candidate state codes
def IPE_r2(F1F2,F2F3,F3F4,F4F5,F5F6,x_cor,y_cor,FLAGS):
  fil_num = FLAGS.fil_num;
  img_pair = tf.concat([F1F2,F2F3,F3F4,F4F5,F5F6],0); 				# [Batch*5,32,32,8]; concatenate tensor pairs along axis 0 (batch number);###### What does Index 0 look like now?
  h_3_2_x_y = tf.concat([img_pair,x_cor,y_cor],3);					# [Batch*5,32,32,10]; concatenate img_pair with x and y coordinate channels along axis 3 (color channel AND coordinate) [R1,G1,B1,0,R2,G2,B2,0,x_cor,y_cor]
  # Fourth conv and max-pooling layers to unit height and width
  w_4_1, b_4_1 = conv_variable([3,3,(FLAGS.col_dim*2) + 2,fil_num]);      				# get weight and bias tensors of 4th convolution 1; fil_num is the number of filters
  h_4_1 = tf.nn.relu(conv2d(h_3_2_x_y,w_4_1,1)+b_4_1); 				# compute a 2D convolution given 4D input (h_3_2_x_y) and filter tensors (w_4_1); tf.nn.relu is a linear rectifier: max(input_tensor_with_bias, 0)
  h_4_1 = maxpool2d(h_4_1);											# performs the k x k max pooling of input tensor (h_4_1)
  w_4_2, b_4_2 = conv_variable([3,3,fil_num,fil_num]);	
  h_4_2 = tf.nn.relu(conv2d(h_4_1,w_4_2,1)+b_4_2+h_4_1);			# convolution 2
  h_4_2 = maxpool2d(h_4_2);											# pooling 2
  w_4_3, b_4_3 = conv_variable([3,3,fil_num,fil_num]);
  h_4_3 = tf.nn.relu(conv2d(h_4_2,w_4_3,1)+b_4_3+h_4_2);			# convolution 3
  h_4_3 = maxpool2d(h_4_3);											# pooling 3
  w_4_4, b_4_4 = conv_variable([3,3,fil_num,fil_num]);
  h_4_4 = tf.nn.relu(conv2d(h_4_3,w_4_4,1)+b_4_4+h_4_3);			# convolution 4 ..... according to the paper, this should have 32 channels instead of 16
  h_4_4 = maxpool2d(h_4_4);											# pooling 4
  w_4_5, b_4_5 = conv_variable([3,3,fil_num,fil_num]);
  h_4_5 = tf.nn.relu(conv2d(h_4_4,w_4_5,1)+b_4_5+h_4_4);			# convolution 5
  h_4_5 = maxpool2d(h_4_5)											# pooling 5
  res_pair = tf.reshape(h_4_5,[-1,fil_num]);						# res_pair has dimensions [batch_num*num_pairs, num_filters]; batch_num = 4, num_pairs = 5
  pair1 = tf.slice(res_pair,[0,0],[FLAGS.batch_num,-1]);					# [4,128]; pair1 includes res_pair[0:batch_num] and has dimensions (batch_num,num_filters)
  pair2 = tf.slice(res_pair,[FLAGS.batch_num,0],[FLAGS.batch_num,-1]);		# pair1 includes res_pair[batch_num:batch_num*2]
  pair3 = tf.slice(res_pair,[FLAGS.batch_num*2,0],[FLAGS.batch_num,-1]);
  pair4 = tf.slice(res_pair,[FLAGS.batch_num*3,0],[FLAGS.batch_num,-1]);
  pair5 = tf.slice(res_pair,[FLAGS.batch_num*4,0],[FLAGS.batch_num,-1]);
  return pair1,pair2,pair3,pair4,pair5;

# VE() is the visual encoder, modified to accept 6 images as input, outputting 4 cascading state codes
def VE(F1,F2,F3,F4,F5,F6,x_cor,y_cor,FLAGS):
  F1F2 = tf.concat([F1,F2],3);															# [Batch,32,32,8]; concatenate tensors F1 and F2 along axis 3 (col_dim[4] = color channel dimension); [R1,G1,B1,0,R2,G2,B2,0]
  F2F3 = tf.concat([F2,F3],3);
  F3F4 = tf.concat([F3,F4],3);
  F4F5 = tf.concat([F4,F5],3);
  F5F6 = tf.concat([F5,F6],3);
  pair1,pair2,pair3,pair4,pair5 = IPE_r1(F1F2,F2F3,F3F4,F4F5,F5F6,x_cor,y_cor,FLAGS);	# [4,128]; find 5 candidate state codes from the 5 pairs of images; ww = [batch, 32]
  concated_pair = tf.concat([pair1,pair2,pair3,pair4,pair5],0);							# [20,128]; SAME AS res_pair
  # shared a linear layer
  #fil_num = FLAGS.fil_num; 		
  fil_num=32; # For IPE_r1, 32 dimension is needed.
  w0 = tf.Variable(tf.truncated_normal([fil_num, FLAGS.No*FLAGS.Ds], stddev=0.1), dtype=tf.float32)		# weights; generates normally distributed values in an array of dimensions (num_filters, num_objects*state_code_dimensions) ie (128, 3*64)
  b0 = tf.Variable(tf.zeros([FLAGS.No*FLAGS.Ds]), dtype=tf.float32)										# biases; dimensions are (num_objects*state_code_dimensions)
  h0 = tf.matmul(concated_pair, w0) + b0																# [20,192]; integrate => height; matrix multiply concated_pair with weights, then add bias (x*w + b)
  enpair1 = tf.reshape(tf.slice(h0,[0,0],[FLAGS.batch_num,-1]),[-1,FLAGS.No,FLAGS.Ds]);					# [4,3,64]; slice the pairs again..., then reshape to (batch_num,num_objects,state_code_dimension)
  enpair2 = tf.reshape(tf.slice(h0,[FLAGS.batch_num,0],[FLAGS.batch_num,-1]),[-1,FLAGS.No,FLAGS.Ds]);
  enpair3 = tf.reshape(tf.slice(h0,[FLAGS.batch_num*2,0],[FLAGS.batch_num,-1]),[-1,FLAGS.No,FLAGS.Ds]);
  enpair4 = tf.reshape(tf.slice(h0,[FLAGS.batch_num*3,0],[FLAGS.batch_num,-1]),[-1,FLAGS.No,FLAGS.Ds]);
  enpair5 = tf.reshape(tf.slice(h0,[FLAGS.batch_num*4,0],[FLAGS.batch_num,-1]),[-1,FLAGS.No,FLAGS.Ds]);
  three1 = tf.concat([enpair1,enpair2],2);																# [4,3,128]; concatenate 2 enpairs along dimension 2 (state_code_dimensions) [enpair1[64],enpair2[64]]
  three2 = tf.concat([enpair2,enpair3],2);																# a "three" is the first version of a statecode for 3 frames
  three3 = tf.concat([enpair3,enpair4],2);
  three4 = tf.concat([enpair4,enpair5],2);
  # shared MLP (multilayer perceptron; feedforward neural network)
  fil_num=64;
  threes = tf.concat([three1,three2,three3,three4],0);													# [16,3,128]; combine all "three"s along their batch axis
  threes = tf.reshape(threes,[-1,FLAGS.Ds*2]);															# [48,128]; reshape to get state_code_dimension as its own axis
  w1 = tf.Variable(tf.truncated_normal([FLAGS.Ds*2, fil_num], stddev=0.1), dtype=tf.float32);				# weights1; dimensions are (state_code_dimension*2, 64)
  b1 = tf.Variable(tf.zeros([fil_num]), dtype=tf.float32);
  h1 = tf.nn.relu(tf.matmul(threes, w1) + b1);															# integrate and fire using a rectifier					
  w2 = tf.Variable(tf.truncated_normal([fil_num, fil_num], stddev=0.1), dtype=tf.float32);						# weights2; dimensions are (64,64)
  b2 = tf.Variable(tf.zeros([fil_num]), dtype=tf.float32);
  h2 = tf.nn.relu(tf.matmul(h1, w2) + b2);
  #h2 = tf.nn.relu(tf.matmul(h1, w2) + b2+h1);															# integrate h1 and fire using rectifier
  w3 = tf.Variable(tf.truncated_normal([fil_num, FLAGS.Ds], stddev=0.1), dtype=tf.float32);					# weights3; dimensions are (64,state_code_dimension)
  b3 = tf.Variable(tf.zeros([FLAGS.Ds]), dtype=tf.float32);
  h3 = tf.matmul(h2, w3) + b3;
  #h3 = tf.matmul(h2, w3) + b3+h2;																		# integrate h2 and fire; also use h2 as an additional bias **********why?
  h3 = tf.reshape(h3,[-1,FLAGS.No,FLAGS.Ds]);															# [16,3,64]; reshape h3 so that num_objects and state_code_dimension have their own axes
  S1 = tf.slice(h3,[0,0,0],[FLAGS.batch_num,-1,-1]);													# [4,3,64]; seperate h3 by batches 1-4; these batches are the cascading state codes
  S2 = tf.slice(h3,[FLAGS.batch_num,0,0],[FLAGS.batch_num,-1,-1]);
  S3 = tf.slice(h3,[FLAGS.batch_num*2,0,0],[FLAGS.batch_num,-1,-1]);
  S4 = tf.slice(h3,[FLAGS.batch_num*3,0,0],[FLAGS.batch_num,-1,-1]);
  return S1,S2,S3,S4;

# core_r1() corresponds to Interactive Network core C_1
def core_r1(S,FLAGS,idx):
  fil_num = 64;
  M = tf.unstack(S,FLAGS.No,1);								# [3,4,64]; unstack state code by the objects axis 
  # Self-Dynamics MLP
  SD_in = tf.reshape(S,[-1,FLAGS.Ds]); 						# [12,64]
  with tf.variable_scope('self-dynamics'+str(idx)):			# scope manager for defining ops that creates variables (layers); "self-dyanmics scope"
    w1 = tf.get_variable('w1',shape=[FLAGS.Ds, fil_num]);	# all variables defined with tf.get_variable are *TRAINABLE*
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(SD_in, w1) + b1);				# rectifier integrate 
    w2 = tf.get_variable('w2',shape=[fil_num, fil_num]);
    b2 = tf.get_variable('b2',shape=[fil_num]);
    h2 = tf.matmul(h1, w2) + b2+h1;							# integrate; also adds h1 as an additional bias??*********
  M_self = tf.reshape(h2,[-1,FLAGS.No,fil_num]);			# [4,3,64]; each object from each batch is included in the self-dynamics MLP
  # Relation MLP
  rel_num = int((FLAGS.No)*(FLAGS.No+1)/2);
  rel_in = np.zeros(rel_num,dtype=object);
  for i in range(rel_num):
    row_idx = int(i/(FLAGS.No-1));
    col_idx=int(i%(FLAGS.No-1));
    rel_in[i]=tf.concat([M[row_idx],M[col_idx]],1);			# concatenate each pair of distinct slots
  #print("rel_in shape:",(tf.stack(list(rel_in),axis=0)).shape.as_list())
  rel_in=tf.concat(list(rel_in),0);							# [24,128]
  with tf.variable_scope('Relation'+str(idx)):
    w1 = tf.get_variable('w1',shape=[FLAGS.Ds*2, fil_num]);
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(rel_in, w1) + b1);
    w2 = tf.get_variable('w2',shape=[fil_num,fil_num]);
    b2 = tf.get_variable('b2',shape=[fil_num]);
    h2 = tf.nn.relu(tf.matmul(h1, w2) + b2+h1);
    w3 = tf.get_variable('w3',shape=[fil_num,fil_num]);
    b3 = tf.get_variable('b3',shape=[fil_num]);
    h3 = tf.matmul(h2, w3) + b3+h2;
  M_rel=np.zeros(rel_num,dtype=object);
  for i in range(rel_num):
    M_rel[i]=tf.slice(h3,[(FLAGS.batch_num)*i,0],[(FLAGS.batch_num),-1]);
  M_rel2=np.zeros(FLAGS.No,dtype=object);
  for i in range(FLAGS.No):
    for j in range(int((FLAGS.No+1)/2)):
      M_rel2[i]+=M_rel[i*(int((FLAGS.No+1)/2))+j];					# sum all Relation MLP slots
  M_rel2=tf.stack(list(M_rel2),1);							# [4,3,64]
  # M_update
  M_update=M_self+M_rel2;									# [4,3,64]; sum all quantities for each slot
  # Affector MLP
  aff_in=tf.reshape(M_update,[-1,fil_num]);					# [12,64]
  with tf.variable_scope('Affector'+str(idx)):
    w1 = tf.get_variable('w1',shape=[fil_num, fil_num]);
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(aff_in, w1) + b1+aff_in);		# [12,64]
    w2 = tf.get_variable('w2',shape=[fil_num,fil_num]);
    b2 = tf.get_variable('b2',shape=[fil_num]);
    h2 = tf.nn.relu(tf.matmul(h1, w2) + b2+h1);
    w3 = tf.get_variable('w3',shape=[fil_num,fil_num]);
    b3 = tf.get_variable('b3',shape=[fil_num]);
    h3 = tf.matmul(h2, w3) + b3+h2;
  M_affect = tf.reshape(h3,[-1,FLAGS.No,fil_num]);			# [4,3,64]
  # Output MLP
  M_i_M_affect = tf.concat([S,M_affect],2);					# [4,3,128]
  out_in=tf.reshape(M_i_M_affect,[-1,FLAGS.Ds+fil_num]);	# [12,128]
  with tf.variable_scope('Output'+str(idx)):
    w1 = tf.get_variable('w1',shape=[FLAGS.Ds+fil_num, fil_num]);
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(out_in, w1) + b1);
    w2 = tf.get_variable('w2',shape=[fil_num,FLAGS.Ds]);
    b2 = tf.get_variable('b2',shape=[FLAGS.Ds]);
    h2 = tf.matmul(h1, w2) + b2;
  h2_out = tf.reshape(h2,[-1,FLAGS.No,FLAGS.Ds]);			# [4,3,64]
  return h2_out;

def core_r2(S,FLAGS,idx):
  fil_num=64;
  M=tf.unstack(S,FLAGS.No,1);
  # Self-Dynamics MLP
  M_self=np.zeros(FLAGS.No,dtype=object);
  for i in range(FLAGS.No):
    with tf.variable_scope('self-dynamics'+str(idx)+"_"+str(i+1)):
      w1 = tf.get_variable('w1',shape=[FLAGS.Ds, fil_num]);
      b1 = tf.get_variable('b1',shape=[fil_num]);
      h1 = tf.nn.relu(tf.matmul(M[i], w1) + b1);
      w2 = tf.get_variable('w2',shape=[fil_num,fil_num]);
      b2 = tf.get_variable('b2',shape=[fil_num]);
      h2 = tf.matmul(h1, w2) + b2;
    M_self[i]=h2;
  # Relation MLP
  rel_in=[];
  for row_idx in range(FLAGS.No):
    for col_idx in range(FLAGS.No):
      if(row_idx!=col_idx):
        rel_in+=[tf.concat([M[row_idx],M[col_idx]],1)];
  rel_out=[];
  for i in range(FLAGS.No):
    rel_in_part=tf.concat(rel_in[i*(FLAGS.No-1):(i+1)*(FLAGS.No-1)],0);
    with tf.variable_scope('Relation'+str(idx)+"_"+str(i+1)):
      w1 = tf.get_variable('w1',shape=[FLAGS.Ds*2, fil_num]);
      b1 = tf.get_variable('b1',shape=[fil_num]);
      h1 = tf.nn.relu(tf.matmul(rel_in_part, w1) + b1);
      w2 = tf.get_variable('w2',shape=[fil_num, fil_num]);
      b2 = tf.get_variable('b2',shape=[fil_num]);
      h2 = tf.nn.relu(tf.matmul(h1, w2) + b2);
      w3 = tf.get_variable('w3',shape=[fil_num,fil_num]);
      b3 = tf.get_variable('b3',shape=[fil_num]);
      h3 = tf.matmul(h2, w3) + b3;
    for j in range(FLAGS.No-1):
      rel_out+=[tf.slice(h3,[(FLAGS.batch_num)*j,0],[(FLAGS.batch_num),-1])];
  M_rel2=np.zeros(FLAGS.No,dtype=object);
  for i in range(FLAGS.No):
    for j in range(FLAGS.No-1):
      M_rel2[i]+=rel_out[i*(FLAGS.No-1)+j];
  # M_update
  M_update=np.zeros(FLAGS.No,dtype=object);
  for i in range(FLAGS.No):
    M_update[i]=M_self[i]+M_rel2[i];
  # Affector MLP
  M_affect=np.zeros(FLAGS.No,dtype=object);
  for i in range(FLAGS.No):
    with tf.variable_scope('Affector'+str(idx)+"_"+str(i+1)):
      w1 = tf.get_variable('w1',shape=[fil_num,fil_num]);
      b1 = tf.get_variable('b1',shape=[fil_num]);
      h1 = tf.nn.relu(tf.matmul(M_update[i], w1) + b1);
      w2 = tf.get_variable('w2',shape=[fil_num,fil_num]);
      b2 = tf.get_variable('b2',shape=[fil_num]);
      h2 = tf.nn.relu(tf.matmul(h1, w2) + b2);
      w3 = tf.get_variable('w3',shape=[fil_num,fil_num]);
      b3 = tf.get_variable('b3',shape=[fil_num]);
      h3 = tf.matmul(h2, w3) + b3;
    M_affect[i]=h3;
  M_affect = tf.stack(list(M_affect),1);
  # Output MLP
  M_i_M_affect = tf.concat([S,M_affect],2);
  out_in=tf.reshape(M_i_M_affect,[-1,FLAGS.Ds+fil_num]);
  with tf.variable_scope('Output'+str(idx)):
    w1 = tf.get_variable('w1',shape=[FLAGS.Ds+fil_num, fil_num]);
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(out_in, w1) + b1);
    w2 = tf.get_variable('w2',shape=[fil_num,FLAGS.Ds]);
    b2 = tf.get_variable('b2',shape=[FLAGS.Ds]);
    h2 = tf.matmul(h1, w2) + b2;
  h2_out = tf.reshape(h2,[-1,FLAGS.No,FLAGS.Ds]);
  return h2_out;

# used for the relational MLP so that all wall-agent pairs share same weights and biases; MAYBE NOT THE RIGHT APPROACH
# https://www.tensorflow.org/versions/r1.0/programmers_guide/variable_scope 
"""variables_dict = {
    "relationMLP_w1": tf.get_variable('w1',shape=[(4*2), 64]),
    "relationMLP_b1": tf.get_variable('b1',shape=[64]),
	"relationMLP_w2": tf.get_variable('w2',shape=[64,64]),
    "relationMLP_b2": tf.get_variable('b2',shape=[64]),
	"relationMLP_w3": tf.get_variable('w3',shape=[64,64]),
    "relationMLP_b3": tf.get_variable('b3',shape=[64])
}#"""
variables_dict = {
    "relationMLP_w1": tf.get_variable('relationMLP_w1',shape=[(64*2), 64]),	# Ds*2
    "relationMLP_b1": tf.get_variable('relationMLP_b1',shape=[64]),
	"relationMLP_w2": tf.get_variable('relationMLP_w2',shape=[64,64]),
    "relationMLP_b2": tf.get_variable('relationMLP_b2',shape=[64]),
	"relationMLP_w3": tf.get_variable('relationMLP_w3',shape=[64,64]),
    "relationMLP_b3": tf.get_variable('relationMLP_b3',shape=[64])
}#"""

def relational_MLP(rel_in,variables_dict=variables_dict):
	h1 = tf.nn.relu(tf.matmul(rel_in, variables_dict["relationMLP_w1"]) + variables_dict["relationMLP_b1"]);
	h2 = tf.nn.relu(tf.matmul(h1, variables_dict["relationMLP_w2"]) + variables_dict["relationMLP_b2"] + h1);
	h3 = tf.matmul(h2, variables_dict["relationMLP_w3"]) + variables_dict["relationMLP_b3"] + h2;
	return h3

# core_r1_ww() corresponds to Interactive Network core; specialized for ww maze
def core_r1_ww(S,FLAGS,idx):
  fil_num = 64;
  M = tf.unstack(S[:,:,:FLAGS.Ds],FLAGS.No,1);							# [3,4,8]; unstack state code by the objects axis 
  # Self-Dynamics MLP
  SD_in = tf.reshape(S[:,0,:],[-1,FLAGS.Ds+FLAGS.ext_num]); 			# [4,64]
  with tf.variable_scope('self-dynamics'+str(idx)):						# scope manager for defining ops that creates variables (layers); "self-dyanmics scope"
    w1 = tf.get_variable('w1',shape=[FLAGS.Ds+FLAGS.ext_num, fil_num]);	# all variables defined with tf.get_variable are *TRAINABLE*
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(SD_in, w1) + b1);							# rectifier integrate 
    w2 = tf.get_variable('w2',shape=[fil_num, fil_num]);
    b2 = tf.get_variable('b2',shape=[fil_num]);
    h2 = tf.matmul(h1, w2) + b2+h1;										# integrate; also adds h1 as an additional bias??********* => h1 is starting point (the rest is offset)
  M_self = tf.reshape(h2,[-1,fil_num]);									# [4,64]; each object from each batch is included in the self-dynamics MLP
  # Relation MLP
  #rel_num = int((FLAGS.No)*(FLAGS.No-1)/2);							#--------------FORCE ALL WALLS TO SHARE SAME RELATIONAL BIAS??
  rel_num = int(FLAGS.No-1);
  rel_in = np.zeros(rel_num,dtype=object);
  for i in range(rel_num):
    row_idx = int(i/(FLAGS.No-1));
    col_idx=int(i%(FLAGS.No-1));
    rel_in[i]=tf.concat([M[row_idx],M[col_idx]],1);						# [pairs[3],4,128]; concatenate each pair of distinct slots; all features are compared;---IDEA: convert coordinates to distance+angle, then the shared weights approach will make sense
  #rel_in=tf.concat(list(rel_in),0);									# [24,128]
  rel_in = tf.stack(list(rel_in),axis=0)								# [pairs[3],4,128]
  with tf.variable_scope('Relation'+str(idx)):							#---IDEA: s1 size to [batch_num,Ds], then feed every relation pair to the same NN; reconstruct M_rel via concat---------
    """w1 = tf.get_variable('w1',shape=[(FLAGS.Ds*2), fil_num]);
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(rel_in, w1) + b1);
    w2 = tf.get_variable('w2',shape=[fil_num,fil_num]);
    b2 = tf.get_variable('b2',shape=[fil_num]);
    h2 = tf.nn.relu(tf.matmul(h1, w2) + b2+h1);
    w3 = tf.get_variable('w3',shape=[fil_num,fil_num]);
    b3 = tf.get_variable('b3',shape=[fil_num]);
    h3 = tf.matmul(h2, w3) + b3+h2;#"""
    h3 = [relational_MLP(rel_in[0,:,:])]
    for i in range(1,rel_num):
      h3 = np.append(h3,[relational_MLP(rel_in[i,:,:])],axis=0)
    #print("h3 shape:",tf.stack(list(h3),axis=0).shape.as_list())
  M_rel = tf.stack(list(h3),axis=0)#"""
  """M_rel=np.zeros(rel_num,dtype=object);
  for i in range(rel_num):
    M_rel[i]=tf.slice(h3,[(FLAGS.batch_num)*i,0],[(FLAGS.batch_num),-1]);#"""
  M_rel2=np.zeros(1,dtype=object);
  for i in range(1):
    for j in range(int((FLAGS.No-1)/2)):
      M_rel2[i]+=M_rel[i*(int((FLAGS.No-1)/2))+j];						# sum all Relation MLP slots
  M_rel2=tf.stack(list(M_rel2),1);										# [4,1,64]
  M_rel2 = tf.reshape(M_rel2, [-1,fil_num])								# [4,64]
  # M_update
  M_update=M_self+M_rel2;												# [4,64]; sum all quantities for each slot
  # Affector MLP
  aff_in=tf.reshape(M_update,[-1,fil_num]);								# [4,64]
  with tf.variable_scope('Affector'+str(idx)):
    w1 = tf.get_variable('w1',shape=[fil_num, fil_num]);
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(aff_in, w1) + b1+aff_in);					# [4,64]
    w2 = tf.get_variable('w2',shape=[fil_num,fil_num]);
    b2 = tf.get_variable('b2',shape=[fil_num]);
    h2 = tf.nn.relu(tf.matmul(h1, w2) + b2+h1);
    w3 = tf.get_variable('w3',shape=[fil_num,fil_num]);
    b3 = tf.get_variable('b3',shape=[fil_num]);
    h3 = tf.matmul(h2, w3) + b3+h2;
  M_affect = tf.reshape(h3,[-1,fil_num]);								# [4,64]
  # Output MLP
  M_i_M_affect = tf.concat([S[:,0,:FLAGS.Ds],M_affect],1);				# [4,128]		
  #M_i_M_affect = tf.concat([S[:,0,:],M_affect],1);
  out_in=tf.reshape(M_i_M_affect,[-1,FLAGS.Ds+fil_num]);				# [12,128]
  #out_in=tf.reshape(M_i_M_affect,[-1,FLAGS.Ds+fil_num+FLAGS.ext_num]);
  with tf.variable_scope('Output'+str(idx)):
    w1 = tf.get_variable('w1',shape=[FLAGS.Ds+fil_num, fil_num]);
    #w1 = tf.get_variable('w1',shape=[FLAGS.Ds+fil_num+FLAGS.ext_num, fil_num]);
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(out_in, w1) + b1);
    w2 = tf.get_variable('w2',shape=[fil_num,FLAGS.Ds]);
    b2 = tf.get_variable('b2',shape=[FLAGS.Ds]);
    h2 = tf.matmul(h1, w2) + b2;
  h2_out = tf.reshape(h2,[-1,1,FLAGS.Ds]);								# [4,1,64]
  h2_out = tf.concat([h2_out, S[:,1:,:FLAGS.Ds]],axis=1)				# [4,3,64]; only predict future state of the moving agent
  return h2_out;
  
  # core_r1_FB() corresponds to Interactive Network core; specialized for FB DVS data
def core_r1_FB(S,FLAGS,idx):
  fil_num = 64;
  M = tf.unstack(S[:,:,:FLAGS.Ds],FLAGS.No,1);							# [3,4,8]; unstack state code by the objects axis 
  # Self-Dynamics MLP
  SD_in = tf.reshape(S[:,0,:],[-1,FLAGS.Ds+FLAGS.ext_num]); 			# [4,64]
  with tf.variable_scope('self-dynamics'+str(idx)):						# scope manager for defining ops that creates variables (layers); "self-dyanmics scope"
    w1 = tf.get_variable('w1',shape=[FLAGS.Ds+FLAGS.ext_num, fil_num]);	# all variables defined with tf.get_variable are *TRAINABLE*
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(SD_in, w1) + b1);							# rectifier integrate 
    w2 = tf.get_variable('w2',shape=[fil_num, fil_num]);
    b2 = tf.get_variable('b2',shape=[fil_num]);
    h2 = tf.matmul(h1, w2) + b2+h1;										# integrate; also adds h1 as an additional bias??********* => h1 is starting point (the rest is offset)
  M_self = tf.reshape(h2,[-1,fil_num]);									# [4,64]; each object from each batch is included in the self-dynamics MLP
  # Relation MLP
  #* ONLY 1 OBJECT TO TRACK, SO NO RELATIONS FOR NOW *#
  
  # M_update
  M_update=M_self#+M_rel2;												# [4,64]; sum all quantities for each slot
  # Affector MLP
  aff_in=tf.reshape(M_update,[-1,fil_num]);								# [4,64]
  with tf.variable_scope('Affector'+str(idx)):
    w1 = tf.get_variable('w1',shape=[fil_num, fil_num]);
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(aff_in, w1) + b1+aff_in);					# [4,64]
    w2 = tf.get_variable('w2',shape=[fil_num,fil_num]);
    b2 = tf.get_variable('b2',shape=[fil_num]);
    h2 = tf.nn.relu(tf.matmul(h1, w2) + b2+h1);
    w3 = tf.get_variable('w3',shape=[fil_num,fil_num]);
    b3 = tf.get_variable('b3',shape=[fil_num]);
    h3 = tf.matmul(h2, w3) + b3+h2;
  M_affect = tf.reshape(h3,[-1,fil_num]);								# [4,64]
  # Output MLP
  M_i_M_affect = tf.concat([S[:,0,:FLAGS.Ds],M_affect],1);				# [4,128]
  #M_i_M_affect = tf.concat([S[:,0,:],M_affect],1);
  out_in=tf.reshape(M_i_M_affect,[-1,FLAGS.Ds+fil_num]);				# [12,128]
  #out_in=tf.reshape(M_i_M_affect,[-1,FLAGS.Ds+fil_num+FLAGS.ext_num]);
  with tf.variable_scope('Output'+str(idx)):
    w1 = tf.get_variable('w1',shape=[FLAGS.Ds+fil_num, fil_num]);
    #w1 = tf.get_variable('w1',shape=[FLAGS.Ds+fil_num+FLAGS.ext_num, fil_num]);
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(out_in, w1) + b1);
    w2 = tf.get_variable('w2',shape=[fil_num,FLAGS.Ds]);
    b2 = tf.get_variable('b2',shape=[FLAGS.Ds]);
    h2 = tf.matmul(h1, w2) + b2;
  h2_out = tf.reshape(h2,[-1,1,FLAGS.Ds]);								# [4,1,64]
  h2_out = tf.concat([h2_out, S[:,1:,:FLAGS.Ds]],axis=1)				# [4,3,64]; only predict future state of the moving agent
  return h2_out;

# DP() applies the dynamics predictor function;
# inputs a sequence of 4 consecutive code states [S1,...,S4] and outputs a predicted state code
def DP(S1,S2,S3,S4,FLAGS):
  Sc1=core_r1_ww(S1,FLAGS,4); 											# [4,3,64]; Interactive Network core C_1
  Sc3=core_r1_ww(S3,FLAGS,2);
  Sc4=core_r1_ww(S4,FLAGS,1);
  fil_num=64;
  # Aggregator MLP
  S=tf.concat([Sc1,Sc3,Sc4],2);											# [4,3,192]
  S=tf.reshape(S[:,0,:],[-1,FLAGS.Ds*3]);								# [12,192]; only aggregate IN results for agent
  with tf.variable_scope("DP"):
    w1 = tf.get_variable('w1',shape=[FLAGS.Ds*3, fil_num]);
    b1 = tf.get_variable('b1',shape=[fil_num]);
    h1 = tf.nn.relu(tf.matmul(S, w1) + b1);
    w2 = tf.get_variable('w2',shape=[fil_num, FLAGS.Ds]);
    b2 = tf.get_variable('b2',shape=[FLAGS.Ds]);
    h2 = tf.matmul(h1, w2) + b2;
  h2 = tf.reshape(h2,[-1,1,FLAGS.Ds]);									# [4,1,64]
  h2 = tf.concat([h2,S4[:,1:,:FLAGS.Ds]],axis=1)						# keep wall coordinates of latest state
  return h2;

# SD() is the state decoder; converts a state code to a state
# a *state* is a list of each object's position/velocity vector
# this is a linear layer with input size L_code and output size 4 (x,y,x_vel,y_vel)		
def SD(output_dp,FLAGS):
  # State Decoder
  w1 = tf.Variable(tf.truncated_normal([FLAGS.Ds, FLAGS.fea_num], stddev=0.1), dtype=tf.float32);
  b1 = tf.Variable(tf.zeros([FLAGS.fea_num]), dtype=tf.float32);
  input_sd=tf.reshape(output_dp,[-1,FLAGS.Ds]);							# [288,64] (originally [96,3,4])
  h1 = tf.matmul(input_sd, w1) + b1;
  h1=tf.reshape(h1,[-1,FLAGS.No,FLAGS.fea_num]);						# [96,3,4]
  return h1;
	
# SE() is the state encoder; converts a state to its state code
# use this on each state; one at a time
def SE(states,FLAGS):
	w1 = tf.Variable(tf.truncated_normal([FLAGS.fea_num, FLAGS.Ds], stddev=0.1), dtype=tf.float32);
	b1 = tf.Variable(tf.zeros([FLAGS.Ds]), dtype=tf.float32);
	states = tf.reshape(states,[-1,FLAGS.fea_num])
	h1 = tf.nn.relu(tf.matmul(states,w1) + b1)
	#h1 = tf.matmul(states,w1) + b1
	h1 = tf.reshape(h1,[-1,FLAGS.No,FLAGS.Ds])							# [batch_num,No,Ds]
	return h1 
	