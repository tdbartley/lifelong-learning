#!/usr/bin/env python
import pickle
import pygame
import numpy as np
#from ple.games.waterworldcircles import WaterWorldCircles
#from ple.games.gridworld import GridWorld
from ple.games.waterworld import WaterWorld

if __name__ == "__main__":
    game = WaterWorld()
    #game = WaterWorldCircles()
    game.screen = pygame.display.set_mode(game.getScreenDims())
    game.clock = pygame.time.Clock()
    game.rng = np.random.RandomState(24)
    #game_state = pickle.load(open("gw.state", "rb"))
    #game.init(game_state)
    game.init()

    while True:
    #for i in range(200):
        if game.game_over():
            game.reset()
        dt = game.clock.tick_busy_loop(30)
        game.step(dt)
        pygame.display.update()
    #pickle.dump(game.getGameState(), open("gw.state", "wb" ))
