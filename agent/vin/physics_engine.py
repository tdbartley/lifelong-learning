from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import os
import numpy as np
import time
from math import sin, cos, radians, pi
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import cv2
from constants import No
from constants import img_folder
from constants import data_folder
from constants import frame_num
from constants import frame_step
from constants import set_num
from constants import fea_num
import cifar10
import cv2 		# used to generate mp4 video 

# G 
#G = 6.67428e-11;
G = 10**5;
# time step
diff_t = 0.01;

# init() sets initial conditions for [mass,x,y,x_vel,y_vel] of each object
# returns an initialized 3D "data" array with the shape [frame_num,n_body,fea_num]
# only data[0,:,:] is set since index 0 is the initial frame
# 	NOTE: norm(data[0][i][1:3]) = sqrt(x_pos^2+y_pos^2), which is equivalent to the variable "distance"
#	Setting orbit=True initializes x, y, x_vel, and y_vel such that the objects will orbit around each other 
#		Otherwise, orbit=False will cause the objects to have slightly more arbitrary initial conditions
def init(frame_num, n_body, fea_num, orbit):
  data = np.zeros((frame_num,n_body,fea_num), dtype=float);
  if(orbit):
    data[0][0][0] = 100;
    data[0][0][1:fea_num] = 0.0;
    for i in range(1, n_body):
      data[0][i][0] = np.random.rand()*8.98 + 0.02;			# init mass of object = random number generated [0,1)*8.98 + 0.02
      distance = np.random.rand()*50.0 + 50.0; 				# all objects are on a grid spanning x=[-100,100], y=[-100,100]; objects start at a distance at least 50 from origin
      theta = np.random.rand()*360;
      theta_rad = pi/2 - radians(theta);    
      data[0][i][1] = distance*cos(theta_rad);				# init x position
      data[0][i][2] = distance*sin(theta_rad);				# init y position 
      data[0][i][3] = -1*data[0][i][2]/norm(data[0][i][1:3])*(G*data[0][0][0]/norm(data[0][i][1:3])**2)*distance/1000;	# init x_vel = (-y/sqrt(x^2+y^2)) * (G*m/(x^2+y^2)) * sqrt(x^2+y^2)/1000
      data[0][i][4] = data[0][i][1]/norm(data[0][i][1:3])*(G*data[0][0][0]/norm(data[0][i][1:3])**2)*distance/1000;		# init y_vel = (x/sqrt(x^2+y^2)) * (G*m/(x^2+y^2)) * sqrt(x^2+y^2)/1000
  else:
    for i in range(n_body):
      data[0][i][0]=np.random.rand()*8.98+0.02;
      distance=np.random.rand()*90.0+10.0;
      theta=np.random.rand()*360;
      theta_rad = pi/2 - radians(theta);    
      data[0][i][1]=distance*cos(theta_rad);
      data[0][i][2]=distance*sin(theta_rad);
      data[0][i][3]=np.random.rand()*6.0-3.0;
      data[0][i][4]=np.random.rand()*6.0-3.0;
  return data;      

# norm(x) returns sqrt(x1^2 + x2^2 + ...)
def norm(x):
  return np.sqrt(np.sum(x**2));

# get_f() returns the x and y forces on reciever contributed by sender
def get_f(reciever, sender):
  diff = sender[1:3] - reciever[1:3];						# difference in x and y locations
  distance = norm(diff);									# calculate distance between the objects
  if(distance < 10):										# limit minimum distance to upper-bound gravitational force
    distance = 10;
  return G*reciever[0]*sender[0]/(distance**3)*diff;

# calc() generates the next frame using Newton's law of gravity  
def calc(cur_state, n_body):
  next_state = np.zeros((n_body,fea_num), dtype=float);
  f_mat = np.zeros((n_body,n_body,2), dtype=float);			# matrix of all contributing forces from each object to each other (x and y)
  f_sum = np.zeros((n_body,2), dtype=float);				# sum of all x and y forces on each object
  acc = np.zeros((n_body,2), dtype=float);					# calculated x and y acceleration on each object
  for i in range(n_body):
    for j in range(i+1, n_body):							# iterate over all object pairs to find their contributing forces
      if(j != i):
        f = get_f(cur_state[i][:3],cur_state[j][:3]);  		# calculate force between 2 objects (pass mass, x_pos, and y_pos)
        f_mat[i,j] += f;									# update force values for both objects (equal and opposite)
        f_mat[j,i] -= f;  
    f_sum[i] = np.sum(f_mat[i],axis=0); 					# sum all x and y forces for each object
    acc[i] = f_sum[i]/cur_state[i][0];
    next_state[i][0]=cur_state[i][0];
    next_state[i][3:5] = cur_state[i][3:5] + acc[i]*diff_t;				# use kinematics equation to find x_vel and y_vel
    next_state[i][1:3] = cur_state[i][1:3] + next_state[i][3:5]*diff_t;	# use kinematics equation to find x_pos and y_pos
  return next_state;

# gen() generates [mass,x,y,x_vel,y_vel] values of each object for all frames
# returns data[frame_numbers, n_body, fea_num]
def gen(n_body, orbit):
  data = init(frame_num*frame_step, n_body, fea_num,orbit); 	# initialization on just first state (ie data[0,:,:])
  for i in range(1, frame_num*frame_step):						# generate [mass,x,y,x_vel,y_vel] of each object for all frames using Newton's laws
    data[i] = calc(data[i-1],n_body);
  data = data[0:frame_num*frame_step:frame_step];				# update first axis of data to label frame numbers 
  return data;

# make_video() combines all frames of a given training/test initial condition into a video 
#	CREDIT: Code fragment taken from Puja Sharma's response in https://stackoverflow.com/questions/33311153/python-extracting-and-saving-video-frames
def make_video(xy, filename, img_folder, prefix):
  fig_num = len(xy)
  image_array = []
  fps = 15
  for i in range(fig_num):
    img = cv2.imread(img_folder + str(prefix) + "_" + str(i) + ".png")
    size =  (img.shape[1],img.shape[0])
    img = cv2.resize(img,size)
    image_array.append(img)
  fourcc = cv2.VideoWriter_fourcc(*'mp4v')				# NOTE: *'mp4v' may not work on your OS; look at error message to find the correct string if this fails
  out = cv2.VideoWriter(filename,fourcc, fps, size)
  
  for i in range(len(image_array)):
    out.write(image_array[i])
  out.release()
  
# make_image() creates and saves all images for the generated frames
def make_image(xy, img_folder, prefix, bg_img):
  if not os.path.exists(img_folder): 								# make directory to store images
    os.makedirs(img_folder);
  fig_num = len(xy); 												# number of figures (which is frame_num*frame_step)
  mydpi = 100;														# dots-per-inch value
  for i in range(fig_num):
    fig = plt.figure(figsize=(32/mydpi,32/mydpi))					# dimensions of figure (inches) 
    plt.xlim(-200, 200)
    plt.ylim(-200, 200)
    plt.axis('off');
    plt.imshow(bg_img, origin='upper', extent=[-200,200,-200,200]);					# place background image behind the plot
    #plt.imshow(cv2.cvtColor(bg_img, cv2.COLOR_BGR2RGB), extent=[-200,200,-200,200])
    color = ['r','b','g','k','y','m','c'];							# color each object (up to 6)
    for j in range(len(xy[0])):
      plt.scatter(xy[i,j,1], xy[i,j,0], c=color[j%len(color)], s=2);# plot all pixels using the scatter-plot function; s=2 is the marker radius (2 pixels); larger values makes it easier to differentiate from the background
    fig.savefig(img_folder+prefix+"_"+str(i)+".png", dpi=mydpi);		# save image
    plt.close()

# same as make_image() but with figures of dimensions 128x128, thicker scatter-plot points, and no background image
# used by gravity_vin.py and ww_vin.py
def make_image2(xy,img_folder,prefix):
  if not os.path.exists(img_folder):
    os.makedirs(img_folder);
  fig_num=len(xy);
  mydpi=100;
  for i in range(fig_num):
    #fig = plt.figure(figsize=(32/mydpi,32/mydpi))
    fig = plt.figure(figsize=(128/mydpi,128/mydpi))
    """plt.xlim(-200, 200)
    plt.ylim(-200, 200)"""
    plt.xlim(-15, 200) #159
    plt.ylim(-15, 200)
    plt.axis('off');
    color=['r','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','b','g','k','y','m','c'];
    for j in range(len(xy[0])):
      #plt.scatter(xy[i,j,1],xy[i,j,0],c=color[j%len(color)],s=0.5);
      plt.scatter(xy[i,j,0],xy[i,j,1],c=color[j%len(color)],s=15);
    fig.savefig(img_folder+prefix+"_"+str(i)+".png",dpi=mydpi);

# make_file() saves data[] into a folder
# format is [num_frames, num_body*fea_num] where each row(num_body*fea_num) is ordered [m1, x1, y1, x_vel1, y_vel1, m2, x2,..., y_vel3]
def make_file(data, data_folder, prefix):
  if not os.path.exists(data_folder):
    os.makedirs(data_folder);
  data = np.array(np.reshape(data,[-1,No*fea_num]), dtype=str);
  f = open(data_folder+prefix+".csv","w");
  for i in range(len(data)):
    f.writelines(",".join(data[i])+"\n");
  
if __name__=='__main__':
  # Get CIFAR 10 dataset
  # 	Combine all data into a 2D array called tr_data_cifar10
  #		All labels combined into 1D array called tr_label_cifar10
  #		Randomized indices stored in 1D array called rand_idx
  cifar10.maybe_download_and_extract();
  cifar_data_dir = "/tmp/cifar10_data/cifar-10-batches-bin/"
  tr_label_cifar10 = np.zeros((50000,1), dtype=float);
  for i in range(1,6): 															# load training data batches 1-5
    file_name = os.path.join(cifar_data_dir,"data_batch_" + str(i)+".bin");
    f = open(file_name,"rb");
    data = np.reshape(bytearray(f.read()),[10000,3073]);						# each file has 10,000 rows of 3073-byte images; bytearray creates array of ints [0,256)
    if(i == 1):
      tr_data_cifar10 = data[:,1:] / 255.0; 									# index 0 is just the image label [0,9]; everything divided by 255 to normalize every value to [0,1]
    else:
      tr_data_cifar10 = np.append(tr_data_cifar10, data[:,1:] / 255.0, axis=0);
    for j in range(len(data)):													# image labels for all 50,000 rows of tr_data_cifar10: labels 0-9 correspond to {airplane, automobile, bird, cat, deer, dog, frog, horse, ship, truck}
      tr_label_cifar10[(i - 1)*10000 + j] = data[j,0];
  rand_idx = list(range(50000)); np.random.shuffle(rand_idx); 					# generate list of random indices for the 50,000 rows
 
  # Making Training Data
  for i in range(set_num):
    img_data = np.reshape(tr_data_cifar10[rand_idx[i]], [3,32,32]); 			# RGB; 32x32 background image
    bg_img = np.dstack((img_data[0,:,:],img_data[1,:,:],img_data[2,:,:]))		# restack along axis 2 to create 3rd dimension (RGB)
	#bg_img=np.ones((32,32,3));													# np.zeros creates black background, np.ones creates background with r,g,b=1
    data = gen(No, True); 														# generate [mass,x,y,x_vel,y_vel] values of each object for all frames
    xy = data[:,:,1:3];
    make_image(xy, img_folder+"train/", str(i), bg_img); 						# create and save all images for the generated frames
    make_file(data,data_folder+"train/",str(i));								# save all frame data into a folder
    make_video(xy, "train_"+str(i)+".mp4", img_folder+"train/", i);				# create and save training videos
	
  # Making Test Data
  img_data = np.reshape(tr_data_cifar10[rand_idx[i]],[3,32,32]);				# test background is the set_num+1 (11'th) image 
  bg_img = np.dstack((img_data[0,:,:],img_data[1,:,:],img_data[2,:,:]))
  data = gen(No, True);
  #bg_img=np.ones((32,32,3));
  xy = data[:,:,1:3];
  make_image(xy,img_folder+"test/",str(0),bg_img);
  make_file(data,data_folder+"test/",str(0));
  make_video(xy, "test.mp4", img_folder+"test/", 0);							# create and save test video
