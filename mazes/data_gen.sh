#!/bin/bash

n_mazes=1
rng_offset=0
n_cpus=1
width=29
height=29
out_path='huge'

let "r=n_mazes / n_cpus"

for i in `seq 0 $r`; do
    for j in `seq 1 $n_cpus`; do
        let "a=$i * $n_cpus + $j + $rng_offset"
        python gen_maze.py $a $width $height $out_path &
    done
    wait
    echo generated $a mazes
done
