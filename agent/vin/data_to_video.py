# generate video data from series of images
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import select 						# allows user to force training to finish and save
import copy
import tensorflow as tf

import math
import matplotlib.image as mpimg
import numpy as np
import time
from vin import VE, DP, SD
from physics_engine import make_video, make_image2
#from foosball_constants import No,img_folder,data_folder,frame_num, set_num, roll_num, fea_num, ext_num, batch_num, num_sessions, save_folder
from ww_constants import No,img_folder,data_folder,frame_num, set_num, roll_num, fea_num, ext_num, batch_num, num_sessions, save_folder
import PIL 		# for horizontally stacking images
import cv2 		# used to generate mp4 video 
#from progress.bar import Bar	# progress bar for training and loading data 
#from ww_in import import_training_data, import_training_data2
from PIL import Image


fig_num = 10#40
filename="real.mp4"
image_array = []
fps = 15
for x in range(350):
  for i in range(3,3+fig_num):
    #list_im = [(img_folder + "track3_DVS_cropped-" + str(i) + ".jpeg"), (img_folder + "track3_orig_cropped-" + str(i) + ".jpeg")]
    #imgs    = [ PIL.Image.open(j) for j in list_im ]
    #min_shape = sorted( [(np.sum(j.size), j.size ) for j in imgs])[0][1] 			# resize all images to smallest image dimensions
    #imgs_comb = np.hstack( (np.asarray( i.resize(min_shape) ) for i in imgs ) )		# horizontally stack the images
    #imgs_comb = PIL.Image.fromarray( imgs_comb)										# save
    #imgs_comb.save(img_folder + "track3_combined-" + str(i) + ".jpeg")
  
    """img = cv2.imread(img_folder + "maze_idx_"+str(x)+"/session_0/" + str(i) + ".png")
    #img = cv2.imread(img_folder + "track3_combined-" + str(i) + ".jpeg") 
    #img = cv2.imread(img_folder + "Single_team1-" +("%03d"%i)+ ".jpeg")
    size = (img.shape[1],img.shape[0])
    img = cv2.resize(img,size)
    image_array.append(img)"""
  #fourcc = cv2.VideoWriter_fourcc(*'mp4v')				# NOTE: *'mp4v' may not work on your OS; look at error message to find the correct string if this fails
  #out = cv2.VideoWriter("maze_references/"+str(x)+"_"+filename,fourcc, fps, size)
  img = cv2.imread(img_folder + "maze_idx_"+str(x)+"/session_0/" + "0" + ".png")
  cv2.imwrite("maze_references/"+str(x)+"_.png",img)
  
"""for i in range(len(image_array)):
  out.write(image_array[i])
out.release()"""

"""inputfile = open("/homes/ehanson/lifelong/agent/vin/foosball_tracking/track3-targetLocations.txt","r")
text = inputfile.readlines()[:] #reads to whole text file, skipping first 0 lines

for line in text:
	line = line.rstrip()		# remove trailing whitespace
	line = line.split(" ")		# split text by space
	if line[0] != "#":			# ignore commented lines
		print(line)				# format of line is [frameNumber, timestamp, x, y, targetTypeID, width, height]#"""