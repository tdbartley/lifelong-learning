Lifelong Learning
=================

This repository contains a set of libraries, environments and agents for lifelong learning in a reinforcement setting. This framework is useful for evaluating sequential multi-task learning or lifelong learning, where the similarity of one task to another must be controlled by the experimenter.


Environments
============

Included are the environments below, which were developed based on PyGame. PyGame Learning Environment enables complete control of the environment, such as level design, game mechanics, graphics and reward signal. It also enables control over what environment states the agent can observe (if any). In addition to the three below, there are several other pre-existing PyGame-based environments which can be found in lib/ple/ple/games/ . There are [thousands of other arcade games written in PyGame](https://www.pygame.org/tags/arcade) to choose from.

GridWorld (simple 2D environments)
----------------

![](https://gitlab.com/tdbartley/lifelong-learning/raw/master/doc/animations/gridworld.gif)

WaterWorld (2D environments with continuous dynamics)
----------------

![](https://gitlab.com/tdbartley/lifelong-learning/raw/master/doc/animations/waterworld.gif)

RaycastMaze (3d environments)
----------------

![](https://gitlab.com/tdbartley/lifelong-learning/raw/master/doc/animations/raycastmaze.gif)


Getting Started
===============


Dependencies
---------------

* [universe-starter-agent dependencies](https://github.com/openai/universe-starter-agent)
* [Conda](https://conda.io/docs/user-guide/install/linux.html)


Software Installation (Ubuntu)
---------------
```
sudo apt-get install -y tmux htop cmake golang libjpeg-dev
```


Software Installation (Arch)
---------------
```
sudo pacman -S tmux htop cmake go opencv lsof
```


Conda Environment Setup and Library Installation
---------------
```
conda create --name rl
source activate rl

# tensorflow for cpu only OR
pip install tensorflow

# tensorflow with gpu support (install cuda first)
pip install tensorflow-gpu

# other python packages
pip install numpy scipy opencv-python pygame
pip install -e lib/universe/ lib/gym/ lib/ple/ lib/gym-ple/
```


Add the following to your `.bashrc` so that you'll have the correct environment when the `train.py` script spawns new bash shells:

```source activate rl```


Basics
======

Train an A3C agent from environment states with without visualizer:

```python train.py```


Train an A3C agent from environment states with with visualizer (only works on localhost not ssh because of OpenGL):

```python train.py -v```


Play test a game:

```python play_game.py```
