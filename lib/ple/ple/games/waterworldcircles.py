from os import listdir
from os.path import isfile, join
import pygame as pg
import sys
import numpy as np
from .base.pygamewrapper import PyGameWrapper
from .utils.vec2d import vec2d
from pygame.constants import K_w, K_a, K_s, K_d


class WaterWorldCircles(PyGameWrapper):
    """
    Parameters
    ----------
    width : int
        Screen width.

    height : int
        Screen height, recommended to be same dimension as width.

    num_creeps : int (default: 3)
        The number of creeps on the screen at once.
    """

    def __init__(self,
                 tile_size=16,
                 grid_width=9,
                 grid_height=9,
                 num_creeps=0,
                 maze_file="mazes/9x9_simple_sorted/17/241.npy"
    ):

        actions = {
            "up": K_w,
            "left": K_a,
            "right": K_d,
            "down": K_s
        }

        screen_width = tile_size * grid_width
        screen_height = tile_size * grid_height

        PyGameWrapper.__init__(self, screen_width, screen_height, actions=actions)
        self.maze_file = maze_file
        self.tile_size = tile_size
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.grid_width = grid_width
        self.grid_height = grid_height
        self.bg_color = (255, 255, 255)
        self.n_creeps = num_creeps
        self.creep_counts = {
            "GOOD": 0
        }
        self.rewards["tick"] = -0.1
        self.agent_speed = tile_size
        self.agent_color = (60, 60, 200)
        self.agent_radius = int(tile_size / 3)
        self.creep_color = (60, 200, 60)
        self.creep_radius = int(tile_size / 3)
        self.wall_color = (10, 10, 10)
        self.wall_radius = int(tile_size / 3)
        self.dx = 0
        self.dy = 0
        self.player = None
        self.creeps = None
        self.walls = None
        self.action_left = 0
        self.action_right = 0
        self.action_up = 0
        self.action_down = 0

    def _handle_player_events(self):
        self.action_left = 0
        self.action_right = 0
        self.action_up = 0
        self.action_down = 0
        self.dx = 0
        self.dy = 0
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()
            if event.type == pg.KEYDOWN:
                key = event.key
                if key == self.actions["left"]:
                    self.action_left = 1
                    self.dx -= self.agent_speed
                if key == self.actions["right"]:
                    self.action_right = 1
                    self.dx += self.agent_speed
                if key == self.actions["up"]:
                    self.action_up = 1
                    self.dy -= self.agent_speed
                if key == self.actions["down"]:
                    self.action_down = 1
                    self.dy += self.agent_speed

    def player_update(self, dx, dy, dt):
        self.player.vel.x += dx
        self.player.vel.y += dy

        old_x = self.player.pos.x
        old_y = self.player.pos.y
        new_x = self.player.pos.x + self.player.vel.x * dt
        new_y = self.player.pos.y + self.player.vel.y * dt

        # x out of bounds
        if new_x > self.player.screen_width - 2 * self.player.radius:
            self.player.pos.x = self.player.screen_width - 2 * self.player.radius
            self.player.vel.x = 0.0
            self.player.vel.y = 0.0
        elif new_x < 0.0:
            self.player.pos.x = 0.0
            self.player.vel.x = 0.0
            self.player.vel.y = 0.0
        else:
            self.player.pos.x = new_x
            self.player.vel.x = self.player.vel.x * 0.975

        # y out of bounds
        if new_y > self.player.screen_height - 2 * self.player.radius:
            self.player.pos.y = self.player.screen_height -2 *  self.player.radius
            self.player.vel.x = 0.0
            self.player.vel.y = 0.0
        elif new_y < 0.0:
            self.player.pos.y = 0.0
            self.player.vel.x = 0.0
            self.player.vel.y = 0.0
        else:
            self.player.pos.y = new_y
            self.player.vel.y = self.player.vel.y * 0.975

        self.player.rect.x = self.player.pos.x
        self.player.rect.y = self.player.pos.y

        # wall collision
        wall_hits = pg.sprite.spritecollide(self.player, self.walls, False, pg.sprite.collide_mask)
        if wall_hits:
            self.player.pos.x = old_x
            self.player.pos.y = old_y
            self.player.vel.x = 0.0
            self.player.vel.y = 0.0

        self.player.rect.x = self.player.pos.x
        self.player.rect.y = self.player.pos.y

    def _add_creep(self):
        creep_init_x = np.random.randint(self.grid_width)
        creep_init_y = np.random.randint(self.grid_height)
        while (self.maze[creep_init_x, creep_init_y] or
               (creep_init_x == self.player_init_x and
                creep_init_y == self.player_init_y)
        ):
            creep_init_x = np.random.randint(self.grid_width)
            creep_init_y = np.random.randint(self.grid_height)
        pos = (self.tile_size * creep_init_x + self.tile_size / 2 - self.creep_radius,
             self.tile_size * creep_init_y + self.tile_size / 2 - self.creep_radius)
        creep = Creep(
            self.creep_color,
            self.creep_radius,
            pos
        )
        self.creeps.add(creep)
        self.creep_counts['GOOD'] += 1

    def getGameState(self):
        """

        Returns
        -------

        dict
            * player x position.
            * player y position.
            * player x velocity.
            * player y velocity.
            * player distance to each creep


        """

        state = {
            "action_left": self.action_left,
            "action_right": self.action_right,
            "action_up": self.action_up,
            "action_down": self.action_down,
            "player_pos_x": self.player.pos.x,
            "player_pos_y": self.player.pos.y,
            "player_vel_x": self.player.vel.x,
            "player_vel_y": self.player.vel.y,
            "wall_pos": []
        }

        for wall in self.walls:
            state["wall_pos"].append(wall.rect.x)
            state["wall_pos"].append(wall.rect.y)
        return state

    def getScore(self):
        return self.score

    def game_over(self):
        """
            Return bool if the game has 'finished'
        """
        #return ((self.creep_counts['GOOD'] == 0) or (self.ticks == 1000))
        return (self.ticks == 1000)

    def init(self):
        """
            Starts/Resets the game to its inital state
        """
        self.creep_counts = {"GOOD": 0}

        # load random maze
        #maze_files = [f for f in listdir(self.data_dir) if isfile(join(self.data_dir, f))]
        #maze_id = maze_files[np.random.randint(0, len(maze_files))]
        self.maze = np.load(self.maze_file)
        print("Initializing maze " + self.maze_file)

        # walls
        if self.walls is None:
            self.walls = pg.sprite.Group()
        else:
            self.walls.empty()
        for i in range(self.grid_width):
            for j in range(self.grid_height):
                if self.maze[i, j]:
                    wall = Wall(
                        self.wall_radius,
                        self.wall_color,
                        (self.tile_size * i + self.tile_size / 2 - self.wall_radius,
                         self.tile_size * j + self.tile_size / 2 - self.wall_radius)
                    )
                    self.walls.add(wall)
        self.walls.draw(self.screen)

        # player
        self.player_init_x = np.random.randint(self.grid_width)
        self.player_init_y = np.random.randint(self.grid_height)
        while self.maze[self.player_init_x, self.player_init_y]:
            self.player_init_x = np.random.randint(self.grid_width)
            self.player_init_y = np.random.randint(self.grid_height)
        self.player = Player(
            self.agent_radius,
            self.agent_color,
            (self.tile_size * self.player_init_x + self.tile_size / 2 - self.agent_radius,
             self.tile_size * self.player_init_y + self.tile_size / 2 - self.agent_radius),
            self.width,
            self.height
        )

        # creeps
        if self.creeps is None:
            self.creeps = pg.sprite.Group()
        else:
            self.creeps.empty()
        for i in range(self.n_creeps):
            self._add_creep()

        self.score = 0
        self.ticks = 0
        self.lives = -1

    def step(self, dt):
        """
            Perform one step of game emulation.
        """
        dt /= 1000.0
        self.ticks += 1

        self.screen.fill(self.bg_color)
        self.score += self.rewards["tick"]
        self._handle_player_events()

        # update player position
        self.player_update(self.dx, self.dy, dt)

        # handle creep collisions
        creep_hits = pg.sprite.spritecollide(self.player, self.creeps, True)
        for creep in creep_hits:
            self.creep_counts["GOOD"] -= 1
            self.score += self.rewards["positive"]

        # win
        if self.creep_counts["GOOD"] == 0:
            self.score += self.rewards["win"]

        # lose
        if self.ticks == 1000:
            self.score += self.rewards["loss"]

        self.walls.draw(self.screen)
        self.creeps.draw(self.screen)
        self.player.draw(self.screen)


class Wall(pg.sprite.Sprite):

    def __init__(self,
                 radius,
                 color,
                 pos_init
    ):
        pg.sprite.Sprite.__init__(self)

        image = pg.Surface([radius * 2, radius * 2])
        image.set_colorkey((0, 0, 0))

        pg.draw.circle(
            image,
            color,
            (radius, radius),
            radius,
            0
        )

        self.image = image.convert()
        self.rect = self.image.get_rect()
        self.radius = radius
        self.mask = pg.mask.from_surface(self.image)
        self.pos = vec2d(pos_init)
        self.rect.x = self.pos.x
        self.rect.y = self.pos.y

    def draw(self, screen):
        screen.blit(self.image, self.rect)


class Player(pg.sprite.Sprite):

    def __init__(self,
                 radius,
                 color,
                 pos_init,
                 screen_width,
                 screen_height
    ):

        pg.sprite.Sprite.__init__(self)

        self.screen_width = screen_width
        self.screen_height = screen_height

        self.pos = vec2d(pos_init)
        self.vel = vec2d((0, 0))

        image = pg.Surface([radius * 2, radius * 2])
        image.set_colorkey((0, 0, 0))

        pg.draw.circle(
            image,
            color,
            (radius, radius),
            radius,
            0
        )

        self.image = image.convert()
        self.rect = self.image.get_rect()
        self.radius = radius
        self.mask = pg.mask.from_surface(self.image)

    def draw(self, screen):
        screen.blit(self.image, self.rect)


class Creep(pg.sprite.Sprite):

    def __init__(self,
                 color,
                 radius,
                 pos_init):

        pg.sprite.Sprite.__init__(self)

        self.pos = vec2d(pos_init)
        self.vel = vec2d((0, 0))

        image = pg.Surface([radius * 2, radius * 2])
        image.set_colorkey((0, 0, 0))

        pg.draw.circle(
            image,
            color,
            (radius, radius),
            radius,
            0
        )

        self.image = image.convert()
        self.rect = self.image.get_rect()
        self.rect.x = pos_init[0]
        self.rect.y = pos_init[1]
        self.radius = radius

    def draw(self, screen):
        screen.blit(self.image, self.rect)


def process_state(state):
    state_list = []
    state_list.append(state["action_left"])
    state_list.append(state["action_right"])
    state_list.append(state["action_up"])
    state_list.append(state["action_down"])
    state_list.append(state["player_pos_x"])
    state_list.append(state["player_pos_y"])
    state_list.append(state["player_vel_x"])
    state_list.append(state["player_vel_y"])
    state_list += state["wall_pos"]
    return np.expand_dims(np.array(state_list, dtype=np.float32), axis=2)


if __name__ == "__main__":
    import numpy as np

    pg.init()
    game = WaterWorldCircles(width=256, height=256, num_creeps=10)
    game.screen = pg.display.set_mode(game.getScreenDims(), 0, 32)
    game.clock = pg.time.Clock()
    game.rng = np.random.RandomState(24)
    game.init()

    while True:
        dt = game.clock.tick_busy_loop(30)
        game.step(dt)
        pg.display.update()
