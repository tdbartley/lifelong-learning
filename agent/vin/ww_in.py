from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import select 													# allows user to force training to finish and save (after current epoch)
import copy
import tensorflow as tf

import math
import matplotlib.image as mpimg
import numpy as np
import time
from vin import VE, DP, SD, SE
from physics_engine import make_video, make_image2
from ww_constants import No,img_folder,data_folder,frame_num, set_num, roll_num, fea_num, ext_num, batch_num, num_sessions, save_folder, set_val, height, weight, col_dim, load_sess, save_sess, pos_norm_const, num_test_inputs, ext_input_val, test_maze, velo_gain
import PIL 														# for horizontally stacking images
import cv2 														# used to generate mp4 video 
from progress.bar import Bar									# progress bar for training and loading data

# Constants that don't need to be touched -----
FLAGS = None													# FLAGS to be passed into vin.py during runtime
x_for_rollout = np.zeros((roll_num,batch_num,No,ext_num))		# external_actions used for rollout
rollout_count = 0												# counter for external actions 
frames_generated = roll_num*num_test_inputs						# frames generated for both true and modeling images (must be smaller value of the two)
first_state = 2													# 3rd frame (idx 2) is the first instance of a generated state
num_labels = 4													# number of labeled states needed to generated rollout
last_frame_offset = -(roll_num + first_state + num_labels) + 1	# last starting frame for batches of a set 
num_pairs = 5													# number of frame pairs generated in the VE to create state code
#----------------------------------------------

# import data values for training
def import_training_vals(dataset_start=0,data_folder=data_folder,frame_num=frame_num):
	total_data = np.zeros((set_num*num_sessions,int(frame_num),(((No-1)*2)+fea_num+ext_num)))		# [set_num*num_sessions*frame_num,(num_walls*2)+fea_num+ext_num]
	for i in range(set_num):
		for j in range(num_sessions):
			for k in range(frame_num):
				temp = np.load(data_folder+"maze_idx_"+str(i+dataset_start)+"/"+"session_"+str(j)+"/"+str(k)+".npy")
				temp = np.reshape(temp,[1,-1])												# reshape each frame to 1 row
				total_data[i*num_sessions + j,k] = temp
	
	total_data[:,:,ext_num:] = total_data[:,:,ext_num:] * (1.0/pos_norm_const)
	total_data[:,:,:ext_num] = total_data[:,:,:ext_num] * ext_input_val
	
	# insert 0 velocity vectors and external_inputs for the walls
	for i in range(16,-1,-1):																# backwards to avoid having to compensate for changing list size
		total_data = np.insert(total_data, (i*2) + 8 + 2, np.zeros(frame_num), axis=2)		# y_vel
		total_data = np.insert(total_data, (i*2) + 8 + 2, np.zeros(frame_num), axis=2)		# x_vel
		
		total_data = np.insert(total_data, (i*2) + 8 + 0, np.zeros(frame_num), axis=2)		# d_input
		total_data = np.insert(total_data, (i*2) + 8 + 0, np.zeros(frame_num), axis=2)		# u_input
		total_data = np.insert(total_data, (i*2) + 8 + 0, np.zeros(frame_num), axis=2)		# r_input
		total_data = np.insert(total_data, (i*2) + 8 + 0, np.zeros(frame_num), axis=2)		# l_input
		#total_data = np.insert(total_data, (i*2) + 8, np.zeros(frame_num), axis=1)			# mass
	
	return total_data																		# [num_of_CIFAR_sets[1],num_frames[1000],num_objects[21]+1,ext_features[4]+num_features[4]]

# import data values for testing
def import_testing_vals(data_folder=data_folder,frame_num=frame_num):
	set_num = 1
	num_sessions = 1
	total_data = np.zeros((set_num*num_sessions,frame_num,(((No-1)*2)+fea_num+ext_num)))		# [set_num*num_sessions*frame_num,(num_walls*2)+fea_num+ext_num]
	for k in range(frame_num):
		temp = np.load(data_folder+"maze_idx_"+str(test_maze)+"/"+"session_"+str(0)+"/"+str(k)+".npy")
		temp = np.reshape(temp,[1,-1])												# reshape each frame to 1 row
		total_data[0,k] = temp
	
	total_data[:,:,ext_num:] = total_data[:,:,ext_num:] * (1.0/pos_norm_const)
	total_data[:,:,:ext_num] = total_data[:,:,:ext_num] * ext_input_val
	
	# insert 0 velocity vectors and external_inputs for the walls
	for i in range(16,-1,-1):																# backwards to avoid having to compensate for changing list size
		total_data = np.insert(total_data, (i*2) + 8 + 2, np.zeros(frame_num), axis=2)		# y_vel
		total_data = np.insert(total_data, (i*2) + 8 + 2, np.zeros(frame_num), axis=2)		# x_vel
	
		total_data = np.insert(total_data, (i*2) + 8 + 0, np.zeros(frame_num), axis=2)		# d_input
		total_data = np.insert(total_data, (i*2) + 8 + 0, np.zeros(frame_num), axis=2)		# u_input
		total_data = np.insert(total_data, (i*2) + 8 + 0, np.zeros(frame_num), axis=2)		# r_input
		total_data = np.insert(total_data, (i*2) + 8 + 0, np.zeros(frame_num), axis=2)		# l_input
		#total_data = np.insert(total_data, (i*2) + 8, np.zeros(frame_num), axis=1)			# mass
	
	return total_data																		# [num_of_CIFAR_sets[1],num_frames[1000],num_objects[21]+1,ext_features[4]+num_features[4]]
	
# import image data for training
def import_train_images(FLAGS,dataset_start=0,img_folder=img_folder):
	total_img = np.zeros((FLAGS.set_num*num_sessions,int(frame_num),FLAGS.height,FLAGS.weight,FLAGS.col_dim),dtype=float);
	for i in range(set_num):
		for j in range(num_sessions):
			for k in range(frame_num):
				total_img[i*num_sessions + j,k] = mpimg.imread(img_folder+"maze_idx_"+str(i+dataset_start)+"/"+"session_"+str(j)+"/"+str(k)+".png")[:,:,:FLAGS.col_dim];
	
	return total_img
	
# import image data for testing
def import_test_images(FLAGS,img_folder=img_folder):
	num_sessions = 1
	set_num = 1
	total_img = np.zeros((FLAGS.set_num*num_sessions,int(frame_num),FLAGS.height,FLAGS.weight,FLAGS.col_dim),dtype=float);
	for k in range(frame_num):
		total_img[0,k] = mpimg.imread(img_folder+"maze_idx_"+str(test_maze)+"/"+"session_"+str(0)+"/"+str(k)+".png")[:,:,:FLAGS.col_dim];
	
	return total_img
	
# import data for training; preferably a substantial amount of data to prevent neural-net overfitting
# contains commented-out code that can be used to incorporate the VE
def import_tr_data(FLAGS,last_frame_offset,dataset_start):
	#total_img = import_train_images(FLAGS,dataset_start)
	total_data = import_training_vals(dataset_start)

	# reshape img and data
	#input_img = np.zeros((FLAGS.set_num*num_sessions*(int(frame_num)+last_frame_offset),first_state+num_labels,FLAGS.height,FLAGS.weight,FLAGS.col_dim),dtype=float);
	output_label = np.zeros((FLAGS.set_num*(int(frame_num)+last_frame_offset),roll_num,FLAGS.No,fea_num),dtype=float);
	output_S_label = np.zeros((FLAGS.set_num*(int(frame_num)+last_frame_offset),num_labels,FLAGS.No,fea_num),dtype=float);
	x_actions = np.zeros((FLAGS.set_num*(int(frame_num)+last_frame_offset),num_labels+roll_num,FLAGS.No,ext_num),dtype=float)
	for i in range(FLAGS.set_num):
		for j in range(int(frame_num)+last_frame_offset):
			#input_img[i*(int(frame_num)+last_frame_offset)+j] = total_img[i,j:j+first_state+num_labels];																									# take all frames up to 6th frame
			output_label[i*(int(frame_num)+last_frame_offset)+j] = np.reshape(total_data[i,j+first_state+num_labels:j+first_state+num_labels+roll_num],[roll_num,FLAGS.No,ext_num+fea_num])[:,:,ext_num:];	# used for predictions after 6th frame
			output_S_label[i*(int(frame_num)+last_frame_offset)+j] = np.reshape(total_data[i,j+first_state:j+first_state+num_labels],[num_labels,FLAGS.No,ext_num+fea_num])[:,:,ext_num:];					# 3 frames produce 1 state label, up to 6th frame
			x_actions[i*(int(frame_num)+last_frame_offset)+j] = np.reshape(total_data[i,j+first_state:j+first_state+num_labels+roll_num],[num_labels+roll_num,FLAGS.No,ext_num+fea_num])[:,:,:ext_num]
	
	lenData = len(output_label)
	tr_data_num = int(lenData*1);
	val_data_num = int(lenData*0);
	total_idx = list(range(lenData));np.random.shuffle(total_idx);
	#mixed_img = input_img[total_idx];
	mixed_label=output_label[total_idx];mixed_S_label=output_S_label[total_idx];mixed_x_actions=x_actions[total_idx]
	#tr_data = mixed_img[:tr_data_num];
	tr_label=mixed_label[:tr_data_num];tr_S_label=mixed_S_label[:tr_data_num];tr_x_actions=mixed_x_actions[:tr_data_num]
	#val_data = mixed_img[tr_data_num:(tr_data_num+val_data_num)];
	val_label=mixed_label[tr_data_num:(tr_data_num+val_data_num)];val_S_label=mixed_S_label[tr_data_num:(tr_data_num+val_data_num)];val_x_actions=mixed_x_actions[tr_data_num:(tr_data_num+val_data_num)]

	#return tr_data, tr_label, tr_S_label, tr_x_actions, lenData
	return tr_label, tr_S_label, tr_x_actions, lenData
	
# import data for testing; preferably data previously unseen by the model during training
# contains commented-out code that can be used to incorporate the VE
def import_test_data(FLAGS,last_frame_offset):
  total_data = import_testing_vals()
  #total_img = import_test_images(FLAGS)
  
  #input_img = np.zeros((1*(int(frame_num)+last_frame_offset),first_state+num_labels,FLAGS.height,FLAGS.weight,FLAGS.col_dim),dtype=float)	
  output_label = np.zeros((1*(int(frame_num)+last_frame_offset),roll_num,FLAGS.No,fea_num),dtype=float);	 							
  output_S_label = np.zeros((1*(int(frame_num)+last_frame_offset),num_labels,FLAGS.No,fea_num),dtype=float);							
  x_actions = np.zeros((FLAGS.set_num*(int(frame_num)+last_frame_offset),num_labels+roll_num,FLAGS.No,ext_num),dtype=float)
  for i in range(1):													
    for j in range(int(frame_num)+last_frame_offset):
      #input_img[i*(int(frame_num)+last_frame_offset)+j] = total_img[i,j:j+first_state+num_labels]; 																									# [37,6,32,32,4]; gather img data from frames (0-36) in batches of 6 and place into input_img[j]
      output_label[i*(int(frame_num)+last_frame_offset)+j] = np.reshape(total_data[i,j+first_state+num_labels:j+first_state+num_labels+roll_num],[roll_num,FLAGS.No,ext_num+fea_num])[:,:,ext_num:]; 	# [37,8,3,4]; collect frames (6-50) in batches of 8 and keep x,y,x_vel,y_vel; [Batches[37],Frames[8],num_object[3],features[4]]
      output_S_label[i*(int(frame_num)+last_frame_offset)+j] = np.reshape(total_data[i,j+first_state:j+first_state+num_labels],[num_labels,FLAGS.No,ext_num+fea_num])[:,:,ext_num:];					# [37,4,3,4]; collect frames (2-42) in batches of 4 and keep x,y,x_vel,y_vel
      x_actions[i*(int(frame_num)+last_frame_offset)+j] = np.reshape(total_data[i,j+first_state:j+first_state+num_labels+roll_num],[num_labels+roll_num,FLAGS.No,ext_num+fea_num])[:,:,:ext_num]

  #return input_img, output_label, output_S_label, x_actions
  return output_label, output_S_label, x_actions
	
# Attach a lot of summaries to a Tensor (for TensorBoard visualization)
def variable_summaries(var,idx):
  with tf.name_scope('summaries_'+str(idx)):
    mean = tf.reduce_mean(var)
    tf.summary.scalar('mean', mean)
    with tf.name_scope('stddev'):
      stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
    tf.summary.scalar('stddev', stddev)
    tf.summary.scalar('max', tf.reduce_max(var))
    tf.summary.scalar('min', tf.reduce_min(var))
    tf.summary.histogram('histogram', var)
	
# make_result_video() combines all frames of a given training/test initial condition into a video 
#	CREDIT: Code fragment taken from Puja Sharma's response in https://stackoverflow.com/questions/33311153/python-extracting-and-saving-video-frames
#	CREDIT: Code for stacking taken from dermen's response in https://stackoverflow.com/questions/30227466/combine-several-images-horizontally-with-python 
def make_result_video(fig_num, filename, img_folder):
	image_array = []
	fps = 15
	for i in range(fig_num):
		list_im = [(img_folder + "true_" + str(i) + ".png"), (img_folder + "modeling_" + str(i) + ".png")]
		imgs    = [ PIL.Image.open(j) for j in list_im ]
		min_shape = sorted( [(np.sum(j.size), j.size ) for j in imgs])[0][1] 			# resize all images to smallest image dimensions
		imgs_comb = np.hstack( (np.asarray( i.resize(min_shape) ) for i in imgs ) )		# horizontally stack the images
		imgs_comb = PIL.Image.fromarray( imgs_comb)										# save
		imgs_comb.save(img_folder + "combined_" + str(i) + ".png")
	
		img = cv2.imread(img_folder + "combined_" + str(i) + ".png")
		size =  (img.shape[1],img.shape[0])
		img = cv2.resize(img,size)
		image_array.append(img)
	fourcc = cv2.VideoWriter_fourcc(*'mp4v')				# NOTE: *'mp4v' may not work on your OS; look at error message to find the correct string if this fails
	out = cv2.VideoWriter(filename,fourcc, fps, size)
  
	for i in range(len(image_array)):
		out.write(image_array[i])
	out.release()

# used by tf.scan to generate rollout frames using the Dynamic Predictor 
def rollout_DP(prev_out,cur_in):
  global x_for_rollout
  global rollout_count
  S1,S2,S3,S4=tf.unstack(prev_out,4,0);
  S_pred = DP(S1,S2,S3,S4,FLAGS);							# [batch_num,No,fea_num]; apply DP function **in vin.py
  new_x_action = x_for_rollout[rollout_count]				# [batch_num,No,fea_num];
  rollout_count += 1
  S_pred = tf.concat([S_pred,new_x_action],axis=2)			# [batch_num,No,fea_num+ext_num]
  res=tf.stack([S2,S3,S4,S_pred],0);						# [states_2_to_pred,batch_num,No,fea_num+ext_num]
  return res;
  
# set up and connect the necessary tensors, optimizer, session, and saver 
# also loads a previously saved model (if the corresponding code is un-commented-out); make sure the "load_sess" value in ww_constants.py is correct 
# contains commented-out code that can be used to incorporate the VE
def setup():
  # Architecture Definition
  # Frames (images)
  F = tf.placeholder(tf.float32, [None,first_state+num_labels,FLAGS.height,FLAGS.weight,FLAGS.col_dim], name="F");	# Inserts a placeholder for a tensor (F) that will be always fed; 
																													# "None" indicates that the first dimension, corresponding to the batch size, can be of any size
  F1,F2,F3,F4,F5,F6 = tf.unstack(F,first_state+num_labels,1);														# [Batch,height,weight,col_dim]; Unpacks the given dimension of a rank-R tensor (F) into 6 rank-(R-1) tensors (F1,F2,F3,F4,F5,F6) across axis 1
																													# Now, F1 = [F_0,F_1,F_2,F_3], F2 = [F_1,F_2,F_3,F_4]

  # External force (user input)
  X = tf.placeholder(tf.float32, [None,num_labels+roll_num,No,ext_num], name="X")		# [num_batches,frames_per_batch,num_objects,up_down_left_right]
  X_part = tf.unstack(X,num_labels+roll_num,1);											# [6,Frame[4],num_objects[3],ext_num[4]]
  
  # Future Data (beyond the 6 input frames)
  label = tf.placeholder(tf.float32, [None,roll_num,No,fea_num], name="label");			# Inserts a placeholder for a tensor (label) that will be always fed
  label_part = tf.unstack(label,roll_num,1);											# [roll_num,batch_num,No,fea_num]; Unpacks the given dimension of a rank-R tensor (label) into 8 rank-(R-1) tensors (label_part) across dimension 1
																						# Now, label_part_1 = [F_0,...F_4], label_part_2 = [F_1,...,F5] and so on (up to 8 batches)
  # Current State Code 
  S_label = tf.placeholder(tf.float32, [None,num_labels,No,fea_num], name="S_label");
  S_label_part = tf.unstack(S_label,num_labels,1);										# [Batch[4],Frame[4],3,4]
  # discount factor
  df = tf.placeholder(tf.float32,[],name="DiscountFactor");								# just a number
  # x and y coordinate channels
  x_cor = tf.placeholder(tf.float32, [None,height,weight,1], name="x_cor");
  y_cor = tf.placeholder(tf.float32, [None,height,weight,1], name="y_cor");

  # x-cor and y-cor setting
  nx, ny = (weight, height);
  x = np.linspace(0, 1, nx);															# distribute coordinates on [0,1) with height or weight increments
  y = np.linspace(0, 1, ny);
  xv, yv = np.meshgrid(x, y);															# [height,weight]; xv and yv have matching indices for each unique coordinate point
  xv = np.reshape(xv,[height,weight,1]);												# [height,weight,1]
  yv = np.reshape(yv,[height,weight,1]);
  xcor = np.zeros((batch_num*num_pairs,height,weight,1),dtype=float);					# [batch_num*num_pairs,height,weight,1]
  ycor = np.zeros((batch_num*num_pairs,height,weight,1),dtype=float);
  for i in range(batch_num*num_pairs):													# for i in range(20); each is a mini-batch
    xcor[i] = xv; ycor[i] = yv;
	
  """# Use Visual Encoder to generate state codes; then append external_actions to them						# uncomment this section to use the VE
  S1,S2,S3,S4 = VE(F1,F2,F3,F4,F5,F6,x_cor,y_cor,FLAGS); 													# [4,3,64]; Visual Encoder defined in vin.py  
  S1 = tf.concat([S1,X_part[0]], axis=2)																	# [batch_num,No,fea_num+ext_num];
  S2 = tf.concat([S2,X_part[1]], axis=2)	
  S3 = tf.concat([S3,X_part[2]], axis=2)	
  S4 = tf.concat([S4,X_part[3]], axis=2) #"""

  #------INSERT STATE CODES 1-4 FROM TRUE DATA; then append external_actions to them--------- 				# uncomment this section to skip the VE and use true states instead
  S1,S2,S3,S4 = tf.unstack(S_label,num_labels,1);															# [batch_num,No,fea_num]; true state; disregard VE for now
  S1 = tf.concat([SE(S1,FLAGS),X_part[0]], axis=2)															# [batch_num,No,fea_num+ext_num]; State Encoder (SE) used to linearly transform states into dimension of state code (Ds) for use in the DP
  S2 = tf.concat([SE(S2,FLAGS),X_part[1]], axis=2)	
  S3 = tf.concat([SE(S3,FLAGS),X_part[2]], axis=2)	
  S4 = tf.concat([SE(S4,FLAGS),X_part[3]], axis=2) #"""
  
  # load external_actions for use in rollout frames
  global x_for_rollout
  global rollout_count
  x_for_rollout = X_part[num_labels:]																		# set up future external actions for rollout frames
  rollout_count = 0
  
  # Rolling Dynamic Predictor
  roll_in = tf.identity(tf.Variable(tf.zeros([roll_num]),dtype=tf.float32));								# create variable 0 tensor of dimension number_of_rollout (20); return it with name "roll_in"
  roll_out = tf.scan(rollout_DP,roll_in,initializer=tf.stack([S1,S2,S3,S4],0));								# [roll_num,4,4,No,Ds]; repeatedly call rollout_DP on values of roll_in; *values* is unpacked roll_in along axis 0
																											# initializer is the initial value for prev_out (return value) specifies output format of rollout_DP
  print("roll_out shape:",roll_out.shape.as_list())
  S_pred = tf.unstack(roll_out[:,:,:,:,:FLAGS.Ds],FLAGS.batch_num,1)[-1]; 									# [20,4,21,4]; grab S_pred from the stack generated by rollout_DP
  S_pred = tf.reshape(tf.stack(tf.unstack(S_pred,FLAGS.batch_num,1),0),[-1,FLAGS.No,FLAGS.Ds]);				# [80,3,64]
  
  # State Decoder																							# S1,S2,S3,S4 are now [4,3,64]
  S = tf.concat([S1[:,:,:FLAGS.Ds],S2[:,:,:FLAGS.Ds],S3[:,:,:FLAGS.Ds],S4[:,:,:FLAGS.Ds],S_pred],0);		# [96,3,64]
  out_sd = SD(S,FLAGS);                     																# [96,3,4]; State Decoder defined in vin.py  
  S_est = np.zeros(num_labels,dtype=object);																# [4,4,3,4]; State_estimate; 4 mini-batches of 4 (ie 0-3, 4-7, 8-11, 12-15)
  for i in range(num_labels):																				# first 4 are the state estimates (S1,S2,S3,S4); all else are the predicted future states
    S_est[i] = tf.slice(out_sd,[FLAGS.batch_num*i,0,0],[FLAGS.batch_num,-1,-1]);					
  label_pred = tf.reshape(tf.slice(out_sd,[FLAGS.batch_num*num_labels,0,0],[-1,-1,-1]),[FLAGS.batch_num,roll_num,FLAGS.No,fea_num]);	# [4,20,3,4]; (batch_num,roll_num,num_objects,xy_characteristics)
  label_pred8 = tf.unstack(label_pred,roll_num,1)[:];																					# [8,4,3,4]; select only 8 of the 20 label_pred
  
  # loss and optimizer (mse = Mean Squared Error - average squared difference)
  mse = df*tf.reduce_mean(tf.reduce_mean(tf.square((label_pred8[0]*pos_norm_const)-(label_part[0]*pos_norm_const)),[1,2])); 			# find mse across axes 1 & 2 (ie across objects and features) #******* print("mse shape:",mse.shape.as_list())
  for i in range(1,roll_num):																				
    mse += (df**(i+1))*tf.reduce_mean(tf.reduce_mean(tf.square((label_pred8[i]*pos_norm_const)-(label_part[i]*pos_norm_const)),[1,2]));	# discount factor compounds (lessens) with each batch compared
  mse = mse/roll_num;																													# total mean
  ve_loss = tf.reduce_mean(tf.reduce_mean(tf.square((S_est[0]*pos_norm_const)-(S_label_part[0]*pos_norm_const)),[1,2]));				# Visual encoder loss; find mse across objects and features
  for i in range(1,num_labels):
    ve_loss += tf.reduce_mean(tf.reduce_mean(tf.square((S_est[i]*pos_norm_const)-(S_label_part[i]*pos_norm_const)),[1,2]));
  ve_loss = ve_loss/num_labels;																											# total VE loss
  total_loss = mse + ve_loss;																		# total loss
  optimizer = tf.train.AdamOptimizer(0.0005); 														# Adam algorithm optimizer; learning rate of 0.0005
  #optimizer = tf.train.MomentumOptimizer(0.0005,0.1); 												# attempt at including the MomentumOptimizer
  trainer = optimizer.minimize(total_loss);	#"""													# set to minimize total_loss; affects all variables defined with tf.get_variable
  
  # tensorboard; enables visualization of the data (must have access to a local web browser)
  # configure Xming (ssh x11 forwarding) to use the web-browser if running this code on a remote computer
  """
  params_list=tf.global_variables();
  for i in range(len(params_list)):
    variable_summaries(params_list[i],i);
  """
  tf.summary.scalar('tr_loss',total_loss);						# Outputs a Summary protocol buffer containing a single scalar value
  merged = tf.summary.merge_all();								# single op that generates all the summary data
  writer = tf.summary.FileWriter(FLAGS.log_dir);				# Writes Summary protocol buffers to event files

  sess = tf.InteractiveSession();								# TensorFlow Session for use in interactive contexts, such as a shell
  tf.global_variables_initializer().run();						# Returns an Op that initializes global variables

  saver = tf.train.Saver()										# Add ops to save and restore all the variables
  
  """saver.restore(sess, save_folder+load_sess)					# Uncomment this code to restore session; Restore variables from disk
  print("Model restored.")#"""
  
  return sess, X, F, label, S_label, df, x_cor, y_cor, xcor, ycor, label_pred, merged, mse, ve_loss, trainer, saver, writer
  
# train a new or existing model using the loaded training data
# also saves the trained model; make sure the "save_sess" value in ww_constants.py is correct 
# also saves training error data into 2 .csv files: one for error per epoch, another for error per batch
# contains commented-out code that can be used to incorporate the VE
def train(sess, X, F, label, S_label, df, x_cor, y_cor, xcor, ycor, label_pred, merged, mse, ve_loss, trainer, saver, writer):
  training_results = [[0.0,0.0,0.0]]																			# [epoch,training_mse,ve_loss]; for use in saving train results
  training_results_interm = [[0.0,0.0,0.0]]
  # beta
  beta = int(FLAGS.max_epoches*0.05);
  # training
  for i in range(FLAGS.max_epoches):
    #df_value=1-math.exp(-1*i/beta);																			# uncomment to use discount_factor_value from IN paper
    df_value = 1;
    tr_loss = 0;
    tr_loss2 = 0;
    #tr_data, tr_label, tr_S_label, tr_x_actions, lenData = import_tr_data(FLAGS,last_frame_offset,0)	# call this function just to get lenData
    tr_label, tr_S_label, tr_x_actions, lenData = import_tr_data(FLAGS,last_frame_offset,0)						# call this function just to get lenData
    bar = Bar('Training_'+str(i), max=int(lenData*set_val/FLAGS.batch_num))										# initialize training bar
    for j in range(set_val):
      #tr_data, tr_label, tr_S_label, tr_x_actions, lenData = import_tr_data(FLAGS,last_frame_offset,set_num*j)
      tr_label, tr_S_label, tr_x_actions, lenData = import_tr_data(FLAGS,last_frame_offset,set_num*j)
      for k in range(int(lenData/FLAGS.batch_num)):
        #batch_data = tr_data[k*FLAGS.batch_num:(k+1)*FLAGS.batch_num];
        batch_label = tr_label[k*FLAGS.batch_num:(k+1)*FLAGS.batch_num];
        batch_S_label = tr_S_label[k*FLAGS.batch_num:(k+1)*FLAGS.batch_num];
        batch_x_actions = tr_x_actions[k*FLAGS.batch_num:(k+1)*FLAGS.batch_num];
        if(k == 0):
          estimated,summary,tr_loss_part,tr_loss_part2,_=sess.run([label,merged,mse,ve_loss,trainer],feed_dict={#F:batch_data,
																												label:batch_label,
																												S_label:batch_S_label,
																												X:batch_x_actions,
																												x_cor:xcor,
																												y_cor:ycor,
																												df:df_value});
          writer.add_summary(summary,i);
        else:
          tr_loss_part,tr_loss_part2,_=sess.run([mse,ve_loss,trainer],feed_dict={#F:batch_data,
																				label:batch_label,
																				S_label:batch_S_label,
																				X:batch_x_actions,
																				x_cor:xcor,
																				y_cor:ycor,
																				df:df_value});
        tr_loss += tr_loss_part;
        tr_loss2 += tr_loss_part2;
        training_results_interm = np.append(training_results_interm,[[i, tr_loss/(int(lenData/FLAGS.batch_num)), tr_loss2/int(lenData/FLAGS.batch_num)]], axis=0)
        bar.next()																	# advance training bar
    bar.finish()																	# finish progress bar
    tr_idx = list(range(lenData));np.random.shuffle(tr_idx);
    #tr_data = tr_data[tr_idx];
    tr_label = tr_label[tr_idx];
    tr_S_label = tr_S_label[tr_idx];
    training_results = np.append(training_results,[[i, tr_loss/(int(len(tr_idx)/FLAGS.batch_num)), tr_loss2/int(len(tr_idx)/FLAGS.batch_num)]], axis=0)
    print("Epoch "+str(i+1)+" Training mse: "+str(tr_loss/(int(len(tr_idx)/FLAGS.batch_num)))+" Training ve loss: "+str(tr_loss2/int(len(tr_idx)/FLAGS.batch_num)));
    
    if select.select([sys.stdin,],[],[],0.0)[0]:									# force training to finish by inputting into terminal
      break ;
  
  # Save the variables to disk.
  save_path = saver.save(sess, save_folder+save_sess)							# uncomment to save model
  print("Model saved in path: %s" % save_path)#"""
  
  training_results = np.delete(training_results, 0, 0)								# delete the dummy first row
  training_results = np.array(training_results,dtype=str)
  f = open("training_metadata.csv","a");											# records VE error and DP MSE per epoch; append to existing csv file
  for i in range(len(training_results)):
    f.writelines(",".join(training_results[i])+"\n");
  
  training_results_interm = np.delete(training_results_interm, 0, 0)				# delete the dummy first row
  training_results_interm = np.array(training_results_interm,dtype=str)
  f = open("training_metadata_interm.csv","a");										# records VE error and DP MSE per batch; append to existing csv file
  for i in range(len(training_results_interm)):
    f.writelines(",".join(training_results_interm[i])+"\n");
	
  return
  
# tests the trained model
# generates corresponding images for true-state and predicted-state of the rollout frames
# also creates a side-by-side video for the true- and predicted-states
# contains commented-out code that can be used to incorporate the VE 
def test(sess, X, F, label, S_label, df, x_cor, y_cor, xcor, ycor, label_pred, merged, mse, ve_loss, trainer, saver):
  output_label, output_S_label, x_actions = import_test_data(FLAGS,last_frame_offset)
  #input_img, output_label, output_S_label, x_actions = import_test_data(FLAGS,last_frame_offset)
  
  xy_origin = output_label[0,:,:,0:2]*pos_norm_const;											# indices 0-2 correspond to XY coordinates of the objects
  for x in range(1,num_test_inputs):
    xy_origin = np.append(xy_origin,output_label[roll_num*x,:,:,0:2]*pos_norm_const,axis=0)
  xy_estimated = np.zeros((roll_num*num_test_inputs,No,2),dtype=float);							# [roll_num,num_objects,x_y[2]]
 
  # Rollout; begin feeding values into session tensors
  # returns [roll_num,No,batch_num]; (roll_num,num_objects,xy_characteristics)
  posi = sess.run(label_pred,feed_dict={#F:input_img[0:batch_num],								# [4,6,32,32,4]; Axis 0 is batch number; ie, batch 0 is {F1,...,F6 = Frames[0-5]}, batch 1 is {F1,...,F6 = Frames[1-6], ...}
										label:output_label[0:batch_num],     					# [4,8,3,4]
										S_label:output_S_label[0:batch_num],					# [4,4,3,4]
										X:x_actions[0:batch_num],								# [4,6,3,4]
										x_cor:xcor,												# [20,32,32,1]
										y_cor:ycor,												# [20,32,32,1]
										df:1.0})[0];  											# only grab results from first batch (out of 4)		
  # Create estimated xy plots for each output of rollout
  #xy_estimated[:roll_num]=posi[:,:,:2]*pos_norm_const;											# uncomment to use predicted XY values instead of predicted velocity
  velo = posi[:,:,2:]*pos_norm_const;															# [20,3,2]; only 2:4 corresponds to x_vel,y_vel
  xy_estimated[0] = (output_S_label[0][3][:,:2]*pos_norm_const)+(velo[0]*velo_gain);			# uncomment to use predicted velocity; first estimated state; true prev. frame + (predicted velocity * velocity constant)
  for i in range(1,len(posi)):
    xy_estimated[i] = xy_estimated[i-1]+(velo[i]*velo_gain);									# uncomment to use predicted velocity
    #xy_estimated[i] = posi[i,:,:2]*pos_norm_const												# uncomment to use predicted XY values instead of predicted velocity
  #print("X_actions:",x_actions[0,:,0,:])														# for user to keep track of the external_inputs used in the rollout frames
  
  #---------
  # for loop continuously feeds values into tensor for all desired rollout frames
  for x in range(1,num_test_inputs):
    posi = sess.run(label_pred,feed_dict={#F:input_img[roll_num*x-1:roll_num*x + batch_num-1],														# [4,6,32,32,4]; Axis 0 is batch number; ie, batch 0 is {F1,...,F6 = Frames[0-5]}, batch 1 is {F1,...,F6 = Frames[1-6], ...}
										label:output_label[roll_num*x-1:roll_num*x + batch_num-1],     												# [4,8,3,4]
										#S_label:output_S_label[roll_num*x-1:roll_num*x + batch_num-1],												# UPDATES ENABLED; uncomment to use true state as updates every rollout
										S_label:np.append([posi[roll_num-4:roll_num]],output_S_label[roll_num*x:roll_num*x + batch_num-1],axis=0),	# UPDATES DISABLED; uncomment to continue off of previous state prediction every rollout
										X:x_actions[roll_num*x-1:roll_num*x + batch_num-1],															# [4,6,3,4]
										x_cor:xcor,																									# [20,32,32,1]
										y_cor:ycor,																									# [20,32,32,1]
										df:1.0})[0];																								# only grab results from first batch (out of 4)
										
    velo = posi[:,:,2:]*pos_norm_const;
    for i in range(len(posi)*x,len(posi)*(x+1)):
      xy_estimated[i] = xy_estimated[i-1]+(velo[i - len(posi)*x]*velo_gain);					# uncomment to use predicted velocity;
      #xy_estimated[i] = posi[i - len(posi)*x,:,:2]*pos_norm_const								# uncomment to use predicted XY values instead of predicted velocity
  #---------
  
  # Saving images and video data
  print("Image Making");
  make_image2(xy_origin,img_folder+"results/","true");
  make_image2(xy_estimated,img_folder+"results/","modeling");
  make_result_video(frames_generated, "result.mp4", img_folder+"results/")
  
  print("Done");
  return

def main(_):
  FLAGS.log_dir += str(int(time.time()));
  if tf.gfile.Exists(FLAGS.log_dir):									# make sure gfile doesnt already exist
    tf.gfile.DeleteRecursively(FLAGS.log_dir)
  tf.gfile.MakeDirs(FLAGS.log_dir)
  FLAGS.No = No;
  FLAGS.set_num = set_num*num_sessions;
  FLAGS.fea_num = fea_num
  FLAGS.ext_num = ext_num
  FLAGS.height = height;
  FLAGS.weight = weight;
  FLAGS.col_dim = col_dim;
  sess, X, F, label, S_label, df, x_cor, y_cor, xcor, ycor, label_pred, merged, mse, ve_loss, trainer, saver, writer = setup()		# setup session; possibly load a saved model
  train(sess, X, F, label, S_label, df, x_cor, y_cor, xcor, ycor, label_pred, merged, mse, ve_loss, trainer, saver, writer)		# train and save model	
  test(sess, X, F, label, S_label, df, x_cor, y_cor, xcor, ycor, label_pred, merged, mse, ve_loss, trainer, saver)					# test the model
  

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--log_dir', type=str, default='/tmp/vin/logs/',
                      help='Summaries log directry')
  parser.add_argument('--batch_num', type=int, default=batch_num,
                      help='The number of data on each mini batch')
  parser.add_argument('--max_epoches', type=int, default=2000,			# default=80,000
                      help='Maximum limitation of epoches')
  parser.add_argument('--Ds', type=int, default=64,
                      help='The State Code Dimension')
  parser.add_argument('--fil_num', type=int, default=128,
                      help='The Number of filters')

  FLAGS, unparsed = parser.parse_known_args()
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)